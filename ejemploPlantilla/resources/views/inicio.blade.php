<!--Para apuntar a un archivo dentro de una carpeta se utiliza el punto-->
@extends('layouts.app')


<!--Creamos el contenido de título dinámico-->
@section('titulo')
    Bienvenido
@endsection


<!--Creamos el contenido del contenido dinámico-->
@section('contenido')
    Este es el contenido de la página principal o la de inicio
@endsection

