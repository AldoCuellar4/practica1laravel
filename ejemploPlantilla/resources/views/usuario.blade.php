<!--Para apuntar a un archivo dentro de una carpeta se utiliza el punto-->
@extends('layouts.app')


<!--Creamos el contenido de título dinámico-->
@section('titulo')
    Bienvenido usuario, ingresa tus datos:)
@endsection


<!--Creamos el contenido del contenido dinámico-->
@section('contenido')
    <body>
        <form action="/my-handling-form-page" method="post">
        <ul>
            <li>
                <label for="nombre">Nombre:</label>
                <input type="text" id="nombre" name="user_nombre" />
            </li>
            <li>
                <label for="edad">Edad:</label>
                <input type="text" id="edad" name="user_edad" />
            </li>
            <li>
                <label for="email">Correo electrónico:</label>
                <input type="email" id="email" name="user_email" />
            </li>

            <li>
                <label for="contraseña">Contraseña:</label>
                <input type="password" id="contraseña" name="user_contraseña">
            </li>
        </ul>
        </form>
    </body>
@endsection
