<?php

namespace App\Http\Controllers;

use App\Models\Venta;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Cliente;
use Illuminate\Http\Request;

class VentaController extends Controller
{
    public function index()
    {
        $ventas = Venta::with(['producto', 'categoria', 'cliente'])->get();
        return view('ventas.ventas', compact('ventas'));
    }

    public function create()
    {
        $productos = Producto::all();
        $categorias = Categoria::all();
        $clientes = Cliente::all();
        return view('ventas.create', compact('productos', 'categorias', 'clientes'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'id_producto' => 'required|exists:productos,id_producto',
            'id_categoria' => 'required|exists:categorias,id_categoria',
            'id_cliente' => 'required|exists:clientes,id_cliente',
            'fecha_venta' => 'required|date',
            'subtotal' => 'required|numeric',
            'iva' => 'required|numeric',
            'total' => 'required|numeric',
        ]);

        Venta::create($request->all());
        return redirect()->route('ventas.index')->with('success', 'Venta añadida exitosamente');
    }

    public function edit(Venta $venta)
    {
        $productos = Producto::all();
        $categorias = Categoria::all();
        $clientes = Cliente::all();
        return view('ventas.edit', compact('venta', 'productos', 'categorias', 'clientes'));
    }

    public function update(Request $request, Venta $venta)
    {
        $request->validate([
            'id_producto' => 'required|exists:productos,id_producto',
            'id_categoria' => 'required|exists:categorias,id_categoria',
            'id_cliente' => 'required|exists:clientes,id_cliente',
            'fecha_venta' => 'required|date',
            'subtotal' => 'required|numeric',
            'iva' => 'required|numeric',
            'total' => 'required|numeric',
        ]);

        $venta->update($request->all());
        return redirect()->route('ventas.index')->with('success', 'Venta actualizada exitosamente');
    }

    public function destroy(Venta $venta)
    {
        $venta->delete();
        return redirect()->route('ventas.index')->with('success', 'Venta eliminada exitosamente');
    }
}
