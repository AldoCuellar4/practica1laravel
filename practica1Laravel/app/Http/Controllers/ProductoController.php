<?php
namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Models\Cliente;
class ProductoController extends Controller
{
    public function index()
    {
        $productos = Producto::with('categoria')->get();
        return view('productos.productos', compact('productos'));
    }

    public function create()
    {
        $productos = Producto::all(); // Aquí obtenemos todos los productos
        $categorias = Categoria::all();
        $clientes = Cliente::all();
        return view('productos.create', compact('productos', 'categorias', 'clientes')); // Pasamos $productos a la vista
    }
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:255',
            'categoria_id' => 'required|exists:categorias,id_categoria',
            'pv' => 'required|numeric',
            'pc' => 'required|numeric',
            'fecha_compra' => 'required|date',
            'colores' => 'required|max:255',
            'desc_Corta' => 'required|max:255',
            'desc_Larga' => 'required'
        ]);

        Producto::create($request->all());
        return redirect()->route('productos.index')->with('success', 'Producto añadido exitosamente');
    }

    public function edit(Producto $producto)
    {
        $categorias = Categoria::all();
        return view('productos.edit', compact('producto', 'categorias'));
    }

    public function update(Request $request, Producto $producto)
    {
        $request->validate([
            'nombre' => 'required|max:255',
            'categoria_id' => 'required|exists:categorias,id_categoria',
            'pv' => 'required|numeric',
            'pc' => 'required|numeric',
            'fecha_compra' => 'required|date',
            'colores' => 'required|max:255',
            'desc_Corta' => 'required|max:255',
            'desc_Larga' => 'required'
        ]);

        $producto->update($request->all());
        return redirect()->route('productos.index')->with('success', 'Producto actualizado exitosamente');
    }

    public function destroy(Producto $producto)
    {
        $producto->delete();
        return redirect()->route('productos.index')->with('success', 'Producto eliminado exitosamente');
    }
}
