<?php

namespace App\Http\Controllers;

use App\Models\Inventario;
use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Http\Request;

class InventarioController extends Controller
{
    public function index(Request $request)
    {
        $query = Inventario::with(['producto', 'categoria']);

        if ($request->has('search')) {
            $search = $request->input('search');
            $query->whereHas('producto', function ($q) use ($search) {
                $q->where('nombre', 'like', '%' . $search . '%');
            });
        }

        $inventarios = $query->get();

        return view('inventarios.inventarios', compact('inventarios'));
    }

    public function create()
    {
        $productos = Producto::all();
        $categorias = Categoria::all();
        return view('inventarios.create', compact('productos', 'categorias'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'id_producto' => 'required|exists:productos,id_producto',
            'id_categoria' => 'required|exists:categorias,id_categoria',
            'fecha_entrada' => 'required|date',
            'fecha_salida' => 'nullable|date',
            'motivo' => 'required|string|max:255',
            'movimiento' => 'required|string|max:255',
            'cantidad' => 'required|integer',
        ]);

        Inventario::create($request->all());
        return redirect()->route('inventarios.index')->with('success', 'Inventario añadido exitosamente');
    }

    public function edit(Inventario $inventario)
    {
        $productos = Producto::all();
        $categorias = Categoria::all();
        return view('inventarios.edit', compact('inventario', 'productos', 'categorias'));
    }

    public function update(Request $request, Inventario $inventario)
    {
        $request->validate([
            'id_producto' => 'required|exists:productos,id_producto',
            'id_categoria' => 'required|exists:categorias,id_categoria',
            'fecha_entrada' => 'required|date',
            'fecha_salida' => 'nullable|date',
            'motivo' => 'required|string|max:255',
            'movimiento' => 'required|string|max:255',
            'cantidad' => 'required|integer',
        ]);

        $inventario->update($request->all());
        return redirect()->route('inventarios.index')->with('success', 'Inventario actualizado exitosamente');
    }

    public function destroy(Inventario $inventario)
    {
        $inventario->delete();
        return redirect()->route('inventarios.index')->with('success', 'Inventario eliminado exitosamente');
    }
}
