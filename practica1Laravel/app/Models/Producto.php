<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
        

    // Establece 'id_producto' como la clave primaria para el modelo Producto
    protected $primaryKey = 'id_producto';
    protected $fillable = [
        'nombre',
        'categoria_id',
        'pv',
        'pc',
        'fecha_compra',
        'colores',
        'desc_Corta',
        'desc_Larga',
    ];
    

    //Función que define una relación de pertenencia a una categoría
    public function categoria()
    {
        // Un producto pertenece a una categoría
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }


    //Función que define una relación de uno a muchos con el modelo Venta
    public function ventas()
    {
        // Un producto puede tener muchas ventas
        return $this->hasMany(Venta::class, 'id_producto');
    }
}
