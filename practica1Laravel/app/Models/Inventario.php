<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_inventario';

    protected $fillable = [
        'id_producto',
        'id_categoria',
        'fecha_entrada',
        'fecha_salida',
        'motivo',
        'movimiento',
        'cantidad',
    ];


    //Función que define una relación de pertenencia a un producto
    public function producto()
    {   
        //El registro de inventario pertecene a un producto
        return $this->belongsTo(Producto::class, 'id_producto');
    }

    //Función que define a una relación de pertenencia a una categoría
    public function categoria()
    {
        //Un registro de inventario pertenece a una categoria 
        return $this->belongsTo(Categoria::class, 'id_categoria');
    }
}
