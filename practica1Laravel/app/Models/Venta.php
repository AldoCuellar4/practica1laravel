<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_producto',
        'id_categoria',
        'id_cliente',
        'fecha_venta',
        'subtotal',
        'iva',
        'total',
    ];

    //Función que hace una relación de pertenencia a un producto
    public function producto()
    {
        //Una venta pertenece a un producto
        return $this->belongsTo(Producto::class, 'id_producto');
    }

    //Función de relación que pertenece a una categoria
    public function categoria()
    {
         // Una venta pertenece a una categoría
        return $this->belongsTo(Categoria::class, 'id_categoria');
    }

    //Función define una relación de pertenencia a un cliente
    public function cliente()
    {
        // Una venta pertenece a un cliente
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }
}
