<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_cliente';

    protected $fillable = [
        'nombre_cliente',
        'correo',
        'telefono',
        'direccion',
        'rfc',
    ];

    //Función que define una relación de uno a muchos con el modelo Venta
    public function ventas()
    {
        // Un cliente puede tener muchas ventas
        return $this->hasMany(Venta::class, 'id_cliente');
    }
}
