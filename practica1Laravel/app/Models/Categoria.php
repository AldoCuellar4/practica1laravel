<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;


    protected $primaryKey = 'id_categoria';

    protected $fillable = [
        'nombre_categoria',
    ];
    
    //Función que define una relación de uno a muchos con el modelo Producto
    public function productos()
    {
        // Una categoría puede tener muchos productos
        return $this->hasMany(Producto::class, 'categoria_id');
    }

    //Función que define una relación de uno a muchos con el modelo Venta
    public function ventas()
    {
        return $this->hasMany(Venta::class, 'id_categoria');
    }
}
