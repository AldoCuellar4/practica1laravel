@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <div class="flex justify-between items-center mb-4">
        <h1 class="text-2xl font-bold">Lista de Productos</h1>
        <a href="{{ route('productos.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Añadir Producto</a>
    </div>
    <div class="bg-white shadow-md rounded my-6">
        <table class="min-w-full table-auto">
            <thead class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                <tr>
                    <th class="py-3 px-6 text-left">Nombre</th>
                    <th class="py-3 px-6 text-left">Categoría</th>
                    <th class="py-3 px-6 text-left">Prod Vendidos</th>
                    <th class="py-3 px-6 text-left">Prod Comprados</th>
                    <th class="py-3 px-6 text-left">Fecha de Compra</th>
                    <th class="py-3 px-6 text-left">Color</th>
                    <th class="py-3 px-6 text-left">Descripción Corta</th>
                    <th class="py-3 px-6 text-left">Descripción Larga</th>
                    <th class="py-3 px-6 text-left">Acciones</th>
                </tr>
            </thead>
            <tbody class="text-gray-600 text-sm font-light">
                @foreach($productos as $producto)
                    <tr class="border-b border-gray-200 hover:bg-gray-100">
                        <td class="py-3 px-6 text-left whitespace-nowrap">{{ $producto->nombre }}</td>
                        <td class="py-3 px-6 text-left">{{ $producto->categoria->nombre_categoria }}</td>
                        <td class="py-3 px-6 text-left">{{ $producto->pv }}</td>
                        <td class="py-3 px-6 text-left">{{ $producto->pc }}</td>
                        <td class="py-3 px-6 text-left">{{ $producto->fecha_compra }}</td>
                        <td class="py-3 px-6 text-left">{{ $producto->colores }}</td>
                        <td class="py-3 px-6 text-left">{{ $producto->desc_Corta }}</td>
                        <td class="py-3 px-6 text-left">{{ $producto->desc_Larga }}</td>
                        <td class="py-3 px-6 text-left">
                            <div class="flex item-center justify-center">
                                <a href="{{ route('productos.edit', $producto->id_producto) }}" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded">Editar</a>
                                <form action="{{ route('productos.destroy', $producto->id_producto) }}" method="POST" class="ml-1">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded">Eliminar</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
