@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <div class="max-w-lg mx-auto bg-white p-8 rounded-lg shadow">
        <h1 class="text-2xl font-bold mb-4">Editar Producto</h1>
        <form action="{{ route('productos.update', $producto->id_producto) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="mb-4">
                <label for="nombre" class="block text-sm font-medium text-gray-700">Nombre</label>
                <input type="text" name="nombre" id="nombre" value="{{ $producto->nombre }}" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="categoria_id" class="block text-sm font-medium text-gray-700">Categoría</label>
                <select name="categoria_id" id="categoria_id" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
                    @foreach($categorias as $categoria)
                        <option value="{{ $categoria->id_categoria }}" {{ $producto->categoria_id == $categoria->id_categoria ? 'selected' : '' }}>{{ $categoria->nombre_categoria }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-4">
                <label for="pv" class="block text-sm font-medium text-gray-700">Productos Vendidos</label>
                <input type="number" name="pv" id="pv" value="{{ $producto->pv }}" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="pc" class="block text-sm font-medium text-gray-700">Productos Comprados</label>
                <input type="number" name="pc" id="pc" value="{{ $producto->pc }}" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="fecha_compra" class="block text-sm font-medium text-gray-700">Fecha de Compra</label>
                <input type="date" name="fecha_compra" id="fecha_compra" value="{{ $producto->fecha_compra }}" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="colores" class="block text-sm font-medium text-gray-700">Colores</label>
                <input type="text" name="colores" id="colores" value="{{ $producto->colores }}" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="desc_Corta" class="block text-sm font-medium text-gray-700">Descripción Corta</label>
                <textarea name="desc_Corta" id="desc_Corta" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>{{ $producto->desc_Corta }}</textarea>
            </div>
            <div class="mb-4">
                <label for="desc_Larga" class="block text-sm font-medium text-gray-700">Descripción Larga</label>
                <textarea name="desc_Larga" id="desc_Larga" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>{{ $producto->desc_Larga }}</textarea>
            </div>
            <div class="flex justify-end">
                <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Guardar</button>
            </div>
        </form>
    </div>
</div>
@endsection
