@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <h1 class="text-2xl font-bold mb-4">Inventario</h1>
    <a href="{{ route('inventarios.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-4 inline-block">Añadir Inventario</a>

    <!-- Formulario de búsqueda -->
    <form action="{{ route('inventarios.index') }}" method="GET" class="mb-4">
        <input type="text" name="search" id="search" value="{{ request('search') }}" placeholder="Buscar producto..." class="border border-gray-300 rounded py-2 px-4">
        <button type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Buscar</button>
        <button type="button" id="clear-search" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Limpiar</button>
    </form>

    <table class="min-w-full bg-white">
        <thead>
            <tr>
                <th class="py-2">Producto</th>
                <th class="py-2">Categoría</th>
                <th class="py-2">Fecha Entrada</th>
                <th class="py-2">Fecha Salida</th>
                <th class="py-2">Motivo</th>
                <th class="py-2">Movimiento</th>
                <th class="py-2">Cantidad</th>
                <th class="py-2">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($inventarios as $inventario)
                <tr>
                    <td class="py-2">{{ $inventario->producto->nombre }}</td>
                    <td class="py-2">{{ $inventario->categoria->nombre_categoria }}</td>
                    <td class="py-2">{{ $inventario->fecha_entrada }}</td>
                    <td class="py-2">{{ $inventario->fecha_salida }}</td>
                    <td class="py-2">{{ $inventario->motivo }}</td>
                    <td class="py-2">{{ $inventario->movimiento }}</td>
                    <td class="py-2">{{ $inventario->cantidad }}</td>
                    <td class="py-2">
                        <a href="{{ route('inventarios.edit', $inventario->id_inventario) }}" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded">Editar</a>
                        <form action="{{ route('inventarios.destroy', $inventario->id_inventario) }}" method="POST" class="inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
document.getElementById('clear-search').addEventListener('click', function() {
    document.getElementById('search').value = '';
    window.location.href = "{{ route('inventarios.index') }}";
});
</script>
@endsection
