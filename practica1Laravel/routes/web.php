<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\VentaController;
use App\Http\Controllers\InventarioController;


//Login
Route::get('/', function () {
    return view('welcome');
});

//Register
Route::get('/register', function () {
    return view('auth.register');
});

//Dashboard
Route::get('/index', [ProductController::class, 'index'])
    ->middleware(['auth', 'verified'])->name('index');

//Index - Lista de productos
Route::get('/index', [ProductController::class, 'index'])
    ->middleware(['auth', 'verified'])->name('index');

    Route::get('/', function () {
        return view('layouts.app');
    });
    
    Route::get('/productos', function () {
        return view('productos.productos');
    });
    
    Route::get('/categorias', function () {
        return view('categorias.categorias');
    });
    
    Route::get('/ventas', function () {
        return view('ventas.ventas');
    });
    
    Route::get('/inventarios', function () {
        return view('inventarios.inventarios');
    });
    
    Route::get('/clientes', function () {
        return view('clientes.clientes');
    });
    
    // Rutas de productos
    Route::resource('productos', ProductoController::class);
    
    // Rutas de categorías
    Route::resource('categorias', CategoriaController::class);
    
    // Rutas de clientes
    Route::resource('clientes', ClienteController::class);
    
    // Rutas de ventas
    Route::resource('ventas', VentaController::class);
    
    // Rutas de inventarios
    Route::resource('inventarios', InventarioController::class);

require __DIR__.'/auth.php';
