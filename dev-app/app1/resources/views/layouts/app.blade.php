<title>Navegación</title>
<body>
    <nav>
        <a href="/" class="href">Pagina principal</a>
        <a href="/alumnos" class="href">Alumnos</a>
    </nav>

    {{-- Agregar directiva de titulo --}}
    <h1>@yield('titulo')</h1>

    {{-- Agregar directiva de contenido --}}
    <h3>@yield('contenido')</h3>
    
</body>