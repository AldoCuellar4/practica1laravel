<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Función ruta para la vista de alumnos
/*
Route::get('/alumnos', function(){
    return view('alumnos');
});
*/

//Otra forma de llamar a la ruta de una manera más sencilla

Route::view('/alumnos','alumnos');