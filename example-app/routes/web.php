<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/usuarios',function(){
    return view('usuarios');
})->middleware(['auth', 'verified'])->name('usuarios');


Route::get('/indexProductos',function(){
    return view('indexProductos');
})->middleware(['auth', 'verified'])->name('indexProductos');


Route::get('/createProducts',function(){
    return view('createProducts');
})->middleware(['auth', 'verified'])->name('createProducts');



Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
