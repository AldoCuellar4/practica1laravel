
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Usuarios') }}
        </h2>
    </x-slot>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Productos') }}
        </h2>
    </x-slot>


    <body>
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900">
                        <form action="/my-handling-form-page" method="post">
                            <ul>
                                <li>
                                    <x-input-label for="nombre" :value="__('Nombre')" />
                                    <x-text-input id="nombre" class="block mt-1 w-full" type="nombre" name="nombre" :value="old('nombre')" required autofocus autocomplete="username" />
                                    <x-input-error :messages="$errors->get('nombre')" class="mt-2" />
                                </li>
                                <li>
                                    <x-input-label for="edad" :value="__('Edad')" />
                                    <x-text-input id="Edad" class="block mt-1 w-full" type="edad" name="edad" :value="old('edad')" required autofocus autocomplete="username" />
                                    <x-input-error :messages="$errors->get('edad')" class="mt-2" />
                                </li>
                                <li>
                                    <x-input-label for="email" :value="__('Email')" />
                                    <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus autocomplete="username" />
                                    <x-input-error :messages="$errors->get('email')" class="mt-2" />
                                </li>
                        
                                <li>
                                    <x-input-label for="password" :value="__('Password')" />
                    
                                    <x-text-input id="password" class="block mt-1 w-full"
                                                    type="password"
                                                    name="password"
                                                    required autocomplete="current-password" />
                    
                                    <x-input-error :messages="$errors->get('password')" class="mt-2" />
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
</x-app-layout>

