<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoEjercicio;

class UsuarioController extends Controller
{
    // Método para mostrar las categorías de ejercicios
    public function categoriaEjercicios()
    {
        $tiposEjercicios = TipoEjercicio::all();
        return view('usuarios.ejercicios.categoriaEjercicios', compact('tiposEjercicios'));
    }

    // Método para mostrar la lista de ejercicios
    public function ejercicios()
    {
        return view('usuarios.ejercicios.ejercicio');
    }
}
