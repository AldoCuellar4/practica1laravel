<?php

namespace App\Http\Controllers;

use App\Models\Ejercicio;
use App\Models\TipoEjercicio;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Validation\Rule;

class EjercicioController extends Controller
{
    public function index()
    {
        $ejercicios = Ejercicio::where('id_tipo', $id_tipo)->get();
        $tipoEjercicio = TipoEjercicio::find($id_tipo);

        return view('ejercicios.index', compact('ejercicios', 'tipoEjercicio'));
    }

    public function create(): View
    {
        $tiposEjercicio = TipoEjercicio::all();
        return view('admin.ejercicios.create', compact('tiposEjercicio'));
    }

    public function store(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'nombre_ejercicio' => 'required|unique:ejercicios,nombre_ejercicio',
            'duracion' => 'nullable|integer',
            'descripcion' => 'nullable|string',
            'id_tipo' => 'nullable|exists:tipo_ejercicio,id_tipo',
            'imagen' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:4096',
        ]);

        if ($request->hasFile('imagen')) {
            $imagenNombre = time() . '.' . $request->imagen->extension();
            $request->imagen->move(public_path('imagenes/ejercicios'), $imagenNombre);
            $validatedData['imagen'] = '/imagenes/ejercicios/' . $imagenNombre;
        }

        Ejercicio::create($validatedData);

        $request->session()->flash('success', 'Ejercicio creado correctamente.');

        return redirect()->route('admin.ejercicios.index');
    }

    public function show(Ejercicio $ejercicio): View
    {
        $ejercicios = Ejercicio::inRandomOrder()->limit(4)->get();
        $ejercicio->load('tipoEjercicio');
        return view('admin.ejercicios.show', compact('ejercicio', 'ejercicios'));
    }

    public function edit(Ejercicio $ejercicio): View
    {
        $tiposEjercicio = TipoEjercicio::all();
        return view('admin.ejercicios.edit', compact('ejercicio', 'tiposEjercicio'));
    }

    public function update(Request $request, Ejercicio $ejercicio): RedirectResponse
    {
        $validatedData = $request->validate([
            'nombre_ejercicio' => [
                'required',
                Rule::unique('ejercicios', 'nombre_ejercicio')->ignore($ejercicio->id)
            ],
            'duracion' => 'nullable|integer',
            'descripcion' => 'nullable|string',
            'id_tipo' => 'nullable|exists:tipo_ejercicio,id_tipo',
            'imagen' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:4096',
        ]);

        if ($request->hasFile('imagen')) {
            // Si ya existe una imagen, puedes eliminarla aquí si es necesario
            $imagenNombre = time() . '.' . $request->imagen->extension();
            $request->imagen->move(public_path('imagenes/ejercicios'), $imagenNombre);
            $validatedData['imagen'] = '/imagenes/ejercicios/' . $imagenNombre;
        }

        $ejercicio->update($validatedData);

        $request->session()->flash('success', 'Ejercicio actualizado correctamente.');

        return redirect()->route('admin.ejercicios.index');
    }

    public function destroy(Ejercicio $ejercicio): RedirectResponse
    {
        // Si el ejercicio tiene una imagen asociada, puedes eliminarla aquí si es necesario
        $ejercicio->delete();
        return redirect()->route('admin.ejercicios.index');
    }
}
