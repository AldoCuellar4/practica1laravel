@extends('layouts.app')

@section('content')
<div class="p-4 sm:ml-60">
    <div class="text-gray-900">
        <div class="p-6 bg-white shadow rounded-lg overflow-hidden">
            <div class="px-4 py-2 flex justify-between items-center bg-gray-100 border-b border-gray-200">
                <h5 class="font-bold text-gray-900 text-lg">Editar Tipo de Ejercicio</h5>
            </div>
            <div class="p-4">
                <!-- Mostrar mensajes de alerta -->
                @if ($errors->any())
                    <div class="mb-4 p-3 bg-red-100 border border-red-400 text-red-700 rounded">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('admin.tipo-ejercicio.update', $tipoEjercicio->id_tipo) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-4">
                        <label for="nombre_tipo" class="block text-gray-700 font-semibold mb-2">Nombre del Tipo de Ejercicio</label>
                        <input type="text" name="nombre_tipo" id="nombre_tipo" class="w-full p-2 border border-gray-300 rounded focus:outline-none focus:ring-2 focus:ring-blue-400" value="{{ old('nombre_tipo', $tipoEjercicio->nombre_tipo) }}" required>
                    </div>
                
                    <div class="flex space-x-2">
                        <button type="submit" class="px-4 py-2 bg-blue-500 text-white font-semibold rounded hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-400">Actualizar</button>
                        <a href="{{ route('admin.tipo-ejercicio.index') }}" class="px-4 py-2 bg-gray-300 text-gray-700 font-semibold rounded hover:bg-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
