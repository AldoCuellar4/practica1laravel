@extends('layouts.app')

@section('content')
<div class="container mx-auto p-4">
    @if (session('success'))
        <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <strong class="font-bold">Éxito:</strong>
            <span class="block sm:inline">{{ session('success') }}</span>
        </div>
    @endif

    @if (session('error'))
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
            <strong class="font-bold">Error:</strong>
            <span class="block sm:inline">{{ session('error') }}</span>
        </div>
    @endif

    <div class="bg-white shadow-md rounded-lg">
        <div class="p-4">
            <h1 class="text-xl font-semibold">Tipos de Ejercicio</h1>

            <div class="mt-4 mb-4">
                <a href="{{ route('admin.tipo-ejercicio.create') }}" class="inline-flex items-center px-4 py-2 bg-[#15616D] text-white text-sm font-medium rounded hover:bg-[#508991] transition duration-150">
                    Crear Nuevo Tipo de Ejercicio
                </a>
            </div>

            <div class="mt-4">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                        <tr>
                            <th class="px-4 py-2">Nombre</th>
                            <th class="px-4 py-2">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200 text-sm">
                        @forelse ($tiposEjercicio as $tipo)
                            <tr>
                                <td class="px-4 py-2">{{ $tipo->nombre_tipo }}</td>
                                <td class="px-4 py-2 text-center">
                                    <a href="{{ route('admin.tipo-ejercicio.edit', $tipo->id_tipo) }}" class="text-blue-500 hover:text-blue-700 mr-2">Editar</a>
                                    <form action="{{ route('admin.tipo-ejercicio.destroy', $tipo->id_tipo) }}" method="POST" class="inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="text-red-500 hover:text-red-700" onclick="return confirm('¿Estás seguro de eliminar este tipo de ejercicio?')">Eliminar</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2" class="px-4 py-2 text-center text-gray-500">No hay tipos de ejercicio disponibles.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

            <!-- Paginación -->
            <div class="mt-6 flex justify-center">
                {{ $tiposEjercicio->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
