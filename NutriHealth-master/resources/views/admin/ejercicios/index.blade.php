@extends('layouts.app')

@section('content')
<div class="container mx-auto p-4">
    <div class="bg-white shadow-md rounded-lg">
        <div class="p-4">
            <h1 class="text-xl font-semibold">Ejercicios</h1>

            <!-- Botón para agregar un nuevo ejercicio -->
            <div class="mb-4">
                <a href="{{ route('admin.ejercicios.create') }}" class="px-4 py-2 bg-blue-500 text-white font-semibold rounded hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-400">Agregar Nuevo Ejercicio</a>
            </div>

            <div class="mt-4">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                        <tr>
                            <th class="px-4 py-2">Nombre</th>
                            <th class="px-4 py-2">Duración</th>
                            <th class="px-4 py-2">Descripción</th>
                            <th class="px-4 py-2">Tipo</th>
                            <th class="px-4 py-2">Imagen</th>
                            <th class="px-4 py-2">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200 text-sm">
                        @forelse ($ejercicios as $ejercicio)
                            <tr>
                                <td class="px-4 py-2">{{ $ejercicio->nombre_ejercicio }}</td>
                                <td class="px-4 py-2 text-center">{{ $ejercicio->duracion }}</td>
                                <td class="px-4 py-2 text-center">{{ $ejercicio->descripcion }}</td>
                                <td class="px-4 py-2 text-center">{{ $ejercicio->tipoEjercicio->nombre_tipo ?? 'N/A' }}</td>
                                <td class="px-4 py-2 text-center">
                                    @if ($ejercicio->imagen)
                                        <img src="{{ asset($ejercicio->imagen) }}" alt="Imagen" class="w-16 h-16 object-cover">
                                    @else
                                        Sin Imagen
                                    @endif
                                </td>
                                <td class="px-4 py-2 text-center">
                                    <a href="{{ route('admin.ejercicios.edit', $ejercicio->id) }}" class="text-blue-500 hover:text-blue-700 mr-2">Editar</a>
                                    <form action="{{ route('admin.ejercicios.destroy', $ejercicio->id) }}" method="POST" class="inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="text-red-500 hover:text-red-700" onclick="return confirm('¿Estás seguro de eliminar este ejercicio?')">Eliminar</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="px-4 py-2 text-center text-gray-500">No hay ejercicios disponibles.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

            <!-- Paginación -->
            <div class="mt-6 flex justify-center">
                {{ $ejercicios->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
