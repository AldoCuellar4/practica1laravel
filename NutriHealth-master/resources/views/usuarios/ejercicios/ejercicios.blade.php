<div class="container mx-auto p-4">
    <h1 class="text-3xl font-bold mb-4">Ejercicios de {{ $tipoEjercicio->nombre_tipo }}</h1>
    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
        @forelse($ejercicios as $ejercicio)
        <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div class="p-5">
                <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $ejercicio->nombre_ejercicio }}</h5>
                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">{{ $ejercicio->descripcion }}</p>
            </div>
        </div>
        @empty
        <p class="text-gray-700">No hay ejercicios disponibles para este tipo.</p>
        @endforelse
    </div>
</div>
