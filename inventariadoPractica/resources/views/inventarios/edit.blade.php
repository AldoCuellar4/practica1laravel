@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <h1 class="text-2xl font-bold mb-4">Editar Inventario</h1>

    <form action="{{ route('inventarios.update', $inventario->id_inventario) }}" method="POST">
        @csrf
        @method('PUT')


        <div class="mb-4">
            <label for="id_producto" class="block text-gray-700">Producto</label>
            <select name="id_producto" id="id_producto" class="w-full border-2 border-gray-300 p-2 rounded" required>

                @foreach($productos as $producto)
                    <option value="{{ $producto->id_producto }}" {{ old('id_producto') == $producto->id_producto ? 'selected' : '' }}>{{ $producto->nombre }}</option>
                @endforeach
            </select>
            @error('id_producto')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-4">
            <label for="id_categoria" class="block text-gray-700">Categoría</label>
            <select name="id_categoria" id="id_categoria" class="w-full border-2 border-gray-300 p-2 rounded" required>
                <option value="" disabled>Seleccione una categoría</option>
                @foreach($categorias as $categoria)
                    <option value="{{ $categoria->id_categoria }}" {{ $inventario->id_categoria == $categoria->id_categoria ? 'selected' : '' }}>{{ $categoria->nombre_categoria }}</option>
                @endforeach
            </select>
            @error('id_categoria')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="fecha_entrada" class="block text-gray-700">Fecha de Entrada</label>
            <input type="date" name="fecha_entrada" id="fecha_entrada" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $inventario->fecha_entrada }}" required>
            @error('fecha_entrada')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="fecha_salida" class="block text-gray-700">Fecha de Salida</label>
            <input type="date" name="fecha_salida" id="fecha_salida" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $inventario->fecha_salida }}">
            @error('fecha_salida')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="motivo" class="block text-gray-700">Motivo</label>
            <input type="text" name="motivo" id="motivo" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $inventario->motivo }}" required>
            @error('motivo')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>



        <div class="mb-4">
            <label for="movimiento" class="block text-gray-700">Movimiento</label>
            <select name="movimiento" id="movimiento" class="w-full border-2 border-gray-300 p-2 rounded" required>
                <option value="Entrada" {{ $inventario->movimiento == 'Entrada' ? 'selected' : '' }}>Entrada</option>
                <option value="Salida" {{ $inventario->movimiento == 'Salida' ? 'selected' : '' }}>Salida</option>
            </select>
            @error('movimiento')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>


        <div class="mb-4">
            <label for="cantidad" class="block text-gray-700">Cantidad</label>
            <input type="text" name="cantidad" id="cantidad" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $inventario->cantidad }}" required>
            @error('cantidad')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Actualizar</button>
    </form>
</div>
@endsection
