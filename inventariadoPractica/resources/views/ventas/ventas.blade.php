@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <h1 class="text-2xl font-bold mb-4">Listado de Ventas</h1>


    <div class="mb-4">
        <a href="{{ route('ventas.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Añadir Venta</a>
    </div>

    <div class="overflow-x-auto">
        <table class="table-auto w-full bg-white shadow-md rounded border-collapse">
            <thead>
                <tr class="bg-gray-200 text-gray-700">
                    <th class="px-4 py-2">Producto</th>
                    <th class="px-4 py-2">Categoría</th>
                    <th class="px-4 py-2">Cliente</th>
                    <th class="px-4 py-2">Fecha de Venta</th>
                    <th class="px-4 py-2">Subtotal</th>
                    <th class="px-4 py-2">IVA</th>
                    <th class="px-4 py-2">Total</th>
                    <th class="px-4 py-2">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ventas as $venta)
                    <tr>
                        <td class="border px-4 py-2">{{ $venta->producto->nombre }}</td>
                        <td class="border px-4 py-2">{{ $venta->categoria->nombre_categoria }}</td>
                        <td class="border px-4 py-2">{{ $venta->cliente->nombre_cliente }}</td>
                        <td class="border px-4 py-2">{{ $venta->fecha_venta }}</td>
                        <td class="border px-4 py-2">{{ $venta->subtotal }}</td>
                        <td class="border px-4 py-2">{{ $venta->iva }}</td>
                        <td class="border px-4 py-2">{{ $venta->total }}</td>
                        <td class="border px-4 py-2">
                            <a href="{{ route('ventas.edit', $venta->id) }}" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded">Editar</a>
                            <form action="{{ route('ventas.destroy', $venta->id) }}" method="POST" class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
