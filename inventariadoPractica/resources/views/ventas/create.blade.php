@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 mt-8">
    <h1 class="text-2xl font-bold mb-4">Añadir Venta</h1>

    <form action="{{ route('ventas.store') }}" method="POST">
        @csrf

        <div class="mb-4">
            <label for="id_cliente" class="block text-gray-700">Cliente</label>
            <select name="id_cliente" id="id_cliente" class="w-full border-2 border-gray-300 p-2 rounded" required>
                <option value="" disabled selected>Seleccione un cliente</option>
                @foreach($clientes as $cliente)
                    <option value="{{ $cliente->id_cliente }}" {{ old('id_cliente') == $cliente->id_cliente ? 'selected' : '' }}>{{ $cliente->nombre_cliente }}</option>
                @endforeach
            </select>
            @error('id_cliente')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="fecha_venta" class="block text-gray-700">Fecha de Venta</label>
            <input type="date" name="fecha_venta" id="fecha_venta" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ old('fecha_venta') }}" required>
            @error('fecha_venta')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <h2 class="text-xl font-bold mb-2">Filtrar por Categoría</h2>
            <div class="flex mb-4">
                <button class="filter-category bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2" data-category-id="all">Todas</button>
                @foreach($categorias as $categoria)
                    <button class="filter-category bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2" data-category-id="{{ $categoria->id_categoria }}">{{ $categoria->nombre_categoria }}</button>
                @endforeach
            </div>
        </div>

        <div class="mb-4">
            <h2 class="text-xl font-bold mb-2">Productos</h2>
            <div id="productos" class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 mb-4">
                @foreach($productos as $producto)
                    <div class="border border-gray-300 p-4 rounded shadow hover:shadow-lg product-card" data-product-id="{{ $producto->id_producto }}" data-product-name="{{ $producto->nombre }}" data-product-price="{{ $producto->pv }}" data-category-id="{{ $producto->categoria->id_categoria }}">
                        <h2 class="text-xl font-bold mb-2">{{ $producto->nombre }}</h2>
                        <p class="text-gray-700 mb-2">Categoría: {{ $producto->categoria->nombre_categoria }}</p>
                        <p class="text-gray-700 mb-2">Precio: ${{ number_format($producto->pv, 2) }}</p>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="mb-4">
            <h2 class="text-xl font-bold mb-2">Productos en el carrito</h2>
            <ul id="cart-list" class="list-disc pl-5 mb-4"></ul>
            <input type="hidden" name="productos" id="productos-input">
        </div>

        <div class="mb-4">
            <label for="subtotal" class="block text-gray-700">Subtotal</label>
            <input type="text" name="subtotal" id="subtotal" class="w-full border-2 border-gray-300 p-2 rounded" readonly required>
            @error('subtotal')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="iva" class="block text-gray-700">IVA</label>
            <input type="text" name="iva" id="iva" class="w-full border-2 border-gray-300 p-2 rounded" readonly required>
            @error('iva')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="total" class="block text-gray-700">Total</label>
            <input type="text" name="total" id="total" class="w-full border-2 border-gray-300 p-2 rounded" readonly required>
            @error('total')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Añadir</button>
    </form>
</div>

<script>
document.addEventListener('DOMContentLoaded', function () {
    const cart = [];
    const subtotalInput = document.getElementById('subtotal');
    const ivaInput = document.getElementById('iva');
    const totalInput = document.getElementById('total');
    const cartList = document.getElementById('cart-list');
    const productosInput = document.getElementById('productos-input');
    const productCards = document.querySelectorAll('.product-card');
    const filterButtons = document.querySelectorAll('.filter-category');

    function updateTotals() {
        const subtotal = cart.reduce((sum, item) => sum + item.price * item.quantity, 0);
        const iva = subtotal * 0.16; // Suponiendo un 16% de IVA
        const total = subtotal + iva;

        subtotalInput.value = subtotal.toFixed(2);
        ivaInput.value = iva.toFixed(2);
        totalInput.value = total.toFixed(2);

        productosInput.value = JSON.stringify(cart);
    }

    function updateCartList() {
        cartList.innerHTML = '';
        cart.forEach(item => {
            const li = document.createElement('li');
            li.classList.add('flex', 'items-center', 'mb-2');
            li.textContent = `${item.name} - $${(item.price * item.quantity).toFixed(2)} x ${item.quantity}`;

            const removeButton = document.createElement('button');
            removeButton.textContent = '-';
            removeButton.classList.add('bg-red-500', 'hover:bg-red-700', 'text-white', 'font-bold', 'py-1', 'px-2', 'rounded', 'ml-2');
            removeButton.addEventListener('click', function () {
                const index = cart.findIndex(cartItem => cartItem.id === item.id);
                if (index !== -1) {
                    cart[index].quantity -= 1;
                    if (cart[index].quantity <= 0) {
                        cart.splice(index, 1);
                    }
                }
                updateTotals();
                updateCartList();
            });

            const addButton = document.createElement('button');
            addButton.textContent = '+';
            addButton.classList.add('bg-green-500', 'hover:bg-green-700', 'text-white', 'font-bold', 'py-1', 'px-2', 'rounded', 'ml-2');
            addButton.addEventListener('click', function () {
                const index = cart.findIndex(cartItem => cartItem.id === item.id);
                if (index !== -1) {
                    cart[index].quantity += 1;
                } else {
                    cart.push({ id: item.id, name: item.name, price: item.price, quantity: 1 });
                }
                updateTotals();
                updateCartList();
            });

            li.appendChild(removeButton);
            li.appendChild(addButton);
            cartList.appendChild(li);
        });
    }

    function filterProductsByCategory(categoryId) {
        productCards.forEach(card => {
            if (categoryId === 'all' || card.dataset.categoryId == categoryId) {
                card.style.display = 'block';
            } else {
                card.style.display = 'none';
            }
        });
    }

    filterButtons.forEach(button => {
        button.addEventListener('click', function () {
            const categoryId = this.dataset.categoryId;
            filterProductsByCategory(categoryId);
        });
    });

    productCards.forEach(card => {
        card.addEventListener('click', function () {
            const productId = this.dataset.productId;
            const productName = this.dataset.productName;
            const productPrice = parseFloat(this.dataset.productPrice);

            const existingItem = cart.find(item => item.id === productId);
            if (existingItem) {
                existingItem.quantity += 1;
            } else {
                cart.push({ id: productId, name: productName, price: productPrice, quantity: 1 });
            }
            updateTotals();
            updateCartList();
        });
    });
});
</script>
@endsection
