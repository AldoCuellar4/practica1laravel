@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <h1 class="text-2xl font-bold mb-4">Editar Venta</h1>

    <form action="{{ route('ventas.update', $venta->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="mb-4">
            <label for="id_producto" class="block text-gray-700">Producto</label>
            <select name="id_producto" id="id_producto" class="w-full border-2 border-gray-300 p-2 rounded" required>

                @foreach($productos as $producto)
                    <option value="{{ $producto->id_producto }}" {{ old('id_producto') == $producto->id_producto ? 'selected' : '' }}>{{ $producto->nombre }}</option>
                @endforeach
            </select>
            @error('id_producto')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="id_categoria" class="block text-gray-700">Categoría</label>
            <select name="id_categoria" id="id_categoria" class="w-full border-2 border-gray-300 p-2 rounded" required>
                @foreach($categorias as $categoria)
                    <option value="{{ $categoria->id_categoria }}" {{ $venta->id_categoria == $categoria->id_categoria ? 'selected' : '' }}>{{ $categoria->nombre_categoria }}</option>
                @endforeach
            </select>
            @error('id_categoria')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="id_cliente" class="block text-gray-700">Cliente</label>
            <select name="id_cliente" id="id_cliente" class="w-full border-2 border-gray-300 p-2 rounded" required>
                @foreach($clientes as $cliente)
                    <option value="{{ $cliente->id_cliente }}" {{ $venta->id_cliente == $cliente->id_cliente ? 'selected' : '' }}>{{ $cliente->nombre_cliente }}</option>
                @endforeach
            </select>
            @error('id_cliente')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="fecha_venta" class="block text-gray-700">Fecha de Venta</label>
            <input type="date" name="fecha_venta" id="fecha_venta" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $venta->fecha_venta }}" required>
            @error('fecha_venta')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="subtotal" class="block text-gray-700">Subtotal</label>
            <input type="text" name="subtotal" id="subtotal" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $venta->subtotal }}" required>
            @error('subtotal')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="iva" class="block text-gray-700">IVA</label>
            <input type="text" name="iva" id="iva" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $venta->iva }}" required>
            @error('iva')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-4">
            <label for="total" class="block text-gray-700">Total</label>
            <input type="text" name="total" id="total" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ $venta->total }}" required>
            @error('total')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>

        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Actualizar</button>
    </form>
</div>
@endsection
