@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <div class="max-w-lg mx-auto bg-white p-8 rounded-lg shadow">
        <h1 class="text-2xl font-bold mb-4">Añadir Categoría</h1>
        <form action="{{ route('categorias.store') }}" method="POST">
            @csrf
            <div class="mb-4">
                <label for="nombre_categoria" class="block text-sm font-medium text-gray-700">Nombre Categoría</label>
                <input type="text" name="nombre_categoria" id="nombre_categoria" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="flex justify-end">
                <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Guardar</button>
            </div>
        </form>
    </div>
</div>
@endsection
