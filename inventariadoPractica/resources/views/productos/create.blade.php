@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <div class="max-w-lg mx-auto bg-white p-8 rounded-lg shadow">
        <h1 class="text-2xl font-bold mb-4">Añadir Producto</h1>
        <form action="{{ route('productos.store') }}" method="POST">
            @csrf
            <div class="mb-4">
                <label for="nombre" class="block text-sm font-medium text-gray-700">Nombre</label>
                <input type="text" name="nombre" id="nombre" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="categoria_id" class="block text-sm font-medium text-gray-700">Categoría</label>
                <select name="categoria_id" id="categoria_id" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
                    <option value="">Seleccione una categoría</option>
                    @foreach($categorias as $categoria)
                        <option value="{{ $categoria->id_categoria }}">{{ $categoria->nombre_categoria }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-4">
                <label for="pv" class="block text-sm font-medium text-gray-700">Precio de venta</label>
                <input type="number" name="pv" id="pv" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="pc" class="block text-sm font-medium text-gray-700">Precio de compra</label>
                <input type="number" name="pc" id="pc" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="fecha_compra" class="block text-sm font-medium text-gray-700">Fecha de Compra</label>
                <input type="date" name="fecha_compra" id="fecha_compra" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="colores" class="block text-sm font-medium text-gray-700">Colores</label>
                <input type="text" name="colores" id="colores" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required>
            </div>
            <div class="mb-4">
                <label for="desc_Corta" class="block text-sm font-medium text-gray-700">Descripción Corta</label>
                <textarea name="desc_Corta" id="desc_Corta" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required></textarea>
            </div>
            <div class="mb-4">
                <label for="desc_Larga" class="block text-sm font-medium text-gray-700">Descripción Larga</label>
                <textarea name="desc_Larga" id="desc_Larga" class="mt-1 p-2 block w-full border border-gray-300 rounded-md shadow-sm focus:ring focus:ring-opacity-50" required></textarea>
            </div>
            <div class="flex justify-end">
                <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Guardar</button>
            </div>
        </form>
    </div>
</div>
@endsection
