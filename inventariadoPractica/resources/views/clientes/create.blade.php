@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <h1 class="text-2xl font-bold mb-4">Añadir Cliente</h1>

    <form action="{{ route('clientes.store') }}" method="POST">
        @csrf
        <div class="mb-4">
            <label for="nombre_cliente" class="block text-gray-700">Nombre</label>
            <input type="text" name="nombre_cliente" id="nombre_cliente" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ old('nombre_cliente') }}" required>
            @error('nombre_cliente')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-4">
            <label for="correo" class="block text-gray-700">Correo</label>
            <input type="email" name="correo" id="correo" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ old('correo') }}" required>
            @error('correo')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-4">
            <label for="telefono" class="block text-gray-700">Teléfono</label>
            <input type="text" name="telefono" id="telefono" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ old('telefono') }}" required pattern="[0-9]+" title="Por favor, ingrese solo números">
            @error('telefono')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-4">
            <label for="direccion" class="block text-gray-700">Dirección</label>
            <input type="text" name="direccion" id="direccion" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ old('direccion') }}" required>
            @error('direccion')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-4">
            <label for="rfc" class="block text-gray-700">RFC</label>
            <input type="text" name="rfc" id="rfc" class="w-full border-2 border-gray-300 p-2 rounded" value="{{ old('rfc') }}" required>
            @error('rfc')
                <div class="text-red-500 mt-2">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Añadir</button>
    </form>
</div>
@endsection
