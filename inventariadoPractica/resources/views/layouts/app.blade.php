<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body class="bg-gray-100">
    <nav class="bg-blue-500 p-4">
        <div class="container mx-auto">
            <div class="flex justify-between items-center">
                <div class="text-white text-lg font-semibold">
                    @auth
                    <span class="text-white px-4">Bienvenido, {{ Auth::user()->name }}</span>
                    <form method="POST" action="{{ route('logout') }}" class="inline">
                        @csrf

                    </form>
                @endauth

                </div>
                <div>
                    <a href="{{ url('/productos') }}" class="text-white px-4">Productos</a>
                    <a href="{{ url('/categorias') }}" class="text-white px-4">Categorias</a>
                    <a href="{{ url('/ventas') }}" class="text-white px-4">Ventas</a>
                    <a href="{{ url('/inventarios') }}" class="text-white px-4">Inventarios</a>
                    <a href="{{ url('/clientes') }}" class="text-white px-4">Clientes</a>
                    <form method="POST" action="{{ route('logout') }}" class="inline">
                        @csrf
                        <button type="submit" class="text-white px-4">Cerrar sesión</button>
                    </form>
                </div>
            </div>
        </div>
    </nav>
    <div class="container mx-auto mt-8">
        @yield('content')
    </div>
    @if(session('success'))
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: '{{ session('success') }}',
            showConfirmButton: false,
            timer: 3000
        });
    </script>
    @endif
</body>
</html>
