<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_categoria';

    protected $fillable = [
        'nombre_categoria',
    ];

    public function productos()
    {
        // Una categoria puede tener muchos productos
        return $this->hasMany(Producto::class, 'id_categoria', 'id_categoria');
    }

    public function ventas()
    {
        // Una categoria puede tener muchas ventas
        return $this->hasMany(Venta::class, 'id_categoria', 'id_categoria');
    }
}
    