<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_producto',
        'id_categoria',
        'id_cliente',
        'fecha_venta',
        'subtotal',
        'iva',
        'total',
    ];

    public function producto()
    {
        //Una venta pertenece a un producto
        return $this->belongsTo(Producto::class, 'id_producto');
    }

    public function categoria()
    {
        //Una venta pertenece a una categoria
        return $this->belongsTo(Categoria::class, 'id_categoria');
    }

    public function cliente()
    {
        //Una venta pertenece a un cliente
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }
}
