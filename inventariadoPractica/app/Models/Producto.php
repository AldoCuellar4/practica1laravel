<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_producto';
    protected $fillable = [
        'nombre',
        'categoria_id',
        'pv',
        'pc',
        'fecha_compra',
        'colores',
        'desc_Corta',
        'desc_Larga',
    ];

    public function categoria()
    {
        //Un producto pertenece a una categoria 
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }

    public function ventas()
    {
        //Un producto puede tener muchas ventas
        return $this->hasMany(Venta::class, 'id_producto');
    }
}
