<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_inventario';

    protected $fillable = [
        'id_producto',
        'id_categoria',
        'fecha_entrada',
        'fecha_salida',
        'motivo',
        'movimiento',
        'cantidad',
    ];

    public function producto()
    {
        //Un registro de inventario pertenece a un producto
        return $this->belongsTo(Producto::class, 'id_producto');
    }

    public function categoria()
    {   
        //Un registro de inventario pertenece a una categoria
        return $this->belongsTo(Categoria::class, 'id_categoria');
    }
}
