<?php
namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        $clientes = Cliente::all();
        return view('clientes.clientes', compact('clientes'));
    }

    public function create()
    {
        $clientes = Cliente::all(); // Obtener todos los clientes
        return view('clientes.create', compact('clientes')); // Pasar $clientes a la vista
    }
    public function store(Request $request)
    {
        $request->validate([
            'nombre_cliente' => 'required|max:255',
            'correo' => 'required|email|unique:clientes',
            'telefono' => 'required|numeric',
            'direccion' => 'required|max:255',
            'rfc' => 'required|unique:clientes|max:13'
        ]);

        Cliente::create($request->all());
        return redirect()->route('clientes.index')->with('success', 'Cliente añadido exitosamente');
    }


    public function edit(Cliente $cliente)
    {
        return view('clientes.edit', compact('cliente'));
    }

    public function update(Request $request, Cliente $cliente)
    {
        $request->validate([
            'nombre_cliente' => 'required|max:255',
            'correo' => 'required|email|unique:clientes,correo,'.$cliente->id_cliente.',id_cliente',
            'telefono' => 'required|numeric|',
            'direccion' => 'required|max:255',
            'rfc' => 'required|unique:clientes,rfc,'.$cliente->id_cliente.',id_cliente|max:13'
        ]);

        $cliente->update($request->all());
        return redirect()->route('clientes.index')->with('success', 'Cliente actualizado exitosamente');
    }

    public function destroy(Cliente $cliente)
    {
        $cliente->delete();
        return redirect()->route('clientes.index')->with('success', 'Cliente eliminado exitosamente');
    }

}
