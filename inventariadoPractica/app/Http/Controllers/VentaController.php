<?php

namespace App\Http\Controllers;

use App\Models\Venta;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Cliente;
use Illuminate\Http\Request;

class VentaController extends Controller
{
    public function index()
    {
        $ventas = Venta::with(['producto', 'categoria', 'cliente'])->get();
        return view('ventas.ventas', compact('ventas'));
    }

    public function create()
    {
        $clientes = Cliente::all();
        $productos = Producto::with('categoria')->get();
        $categorias = Categoria::all();

        return view('ventas.create', compact('clientes', 'productos', 'categorias'));
    }

    public function store(Request $request)
{
    $request->validate([
        'id_cliente' => 'required|exists:clientes,id_cliente',
        'fecha_venta' => 'required|date',
        'productos' => 'required|array',
        'productos.*.id' => 'required|exists:productos,id_producto',
        'productos.*.cantidad' => 'required|integer|min:1',
    ]);

    $venta = new Venta();
    $venta->id_cliente = $request->id_cliente;
    $venta->fecha_venta = $request->fecha_venta;
    $venta->subtotal = 0;  // Se calculará posteriormente
    $venta->iva = 0;       // Se calculará posteriormente
    $venta->total = 0;     // Se calculará posteriormente
    $venta->save();

    $subtotal = 0;
    foreach ($request->productos as $item) {
        $producto = Producto::find($item['id']);
        $subtotal += $producto->pv * $item['cantidad'];
        $venta->productos()->attach($item['id'], ['cantidad' => $item['cantidad']]);
    }

    $iva = $subtotal * 0.16;  // Suponiendo un 16% de IVA
    $total = $subtotal + $iva;

    $venta->subtotal = $subtotal;
    $venta->iva = $iva;
    $venta->total = $total;
    $venta->save();

    return redirect()->route('ventas.index')->with('success', 'Venta añadida exitosamente');
}


    public function edit(Venta $venta)
    {
        $productos = Producto::all();
        $categorias = Categoria::all();
        $clientes = Cliente::all();
        return view('ventas.edit', compact('venta', 'productos', 'categorias', 'clientes'));
    }

    public function update(Request $request, Venta $venta)
    {
        $request->validate([
            'id_producto' => 'required|exists:productos,id_producto',
            'id_categoria' => 'required|exists:categorias,id_categoria',
            'id_cliente' => 'required|exists:clientes,id_cliente',
            'fecha_venta' => 'required|date',
            'subtotal' => 'required|numeric',
            'iva' => 'required|numeric',
            'total' => 'required|numeric',
        ]);

        $venta->update($request->all());
        return redirect()->route('ventas.index')->with('success', 'Venta actualizada exitosamente');
    }

    public function destroy(Venta $venta)
    {
        $venta->delete();
        return redirect()->route('ventas.index')->with('success', 'Venta eliminada exitosamente');
    }
}
