<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    use HasFactory;

    protected $table = 'proveedores'; 

    protected $fillable = [
        'nombre_contacto',
        'correo',
        'telefono',
        'direccion',
    ];

    // Relación uno-a-muchos con Compra
    public function compras()
    {
        return $this->hasMany(Compra::class, 'id_proveedor');
    }
}
