<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    use HasFactory;

    protected $table = 'vendedores';

    protected $fillable = [
        'nombre', 
        'correo', 
        'telefono'
    ];

    public function ventas()
    {
        return $this->hasMany(Venta::class, 'id_vendedor');
    }

    public function tieneVentas()
    {
        return $this->ventas()->exists();
    }

    public function puedeEliminar()
    {
        return !$this->tieneVentas();
    }
}
