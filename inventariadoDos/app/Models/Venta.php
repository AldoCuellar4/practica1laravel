<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id_vendedor',
        'id_cliente',
        'monto',
        'fecha_venta',
    ];
    
    public function vendedor()
    {
        return $this->belongsTo(Vendedor::class, 'id_vendedor');
    }
    
    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }
    
    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'venta_producto', 'venta_id', 'producto_id')
            ->withPivot('cantidad', 'precio')
            ->withTimestamps();
    }
    
    
}

