<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    use HasFactory;
    
    protected $table = 'cotizaciones';
    
    protected $fillable = [
        'id_cliente',
        'total',
        'fecha_cot',
        'comentarios',
    ];
    
    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }
    
    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'cotizacion_producto', 'cotizacion_id', 'producto_id')
                    ->withPivot('cantidad', 'precio');
    }
}
