<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'nombre',
        'correo',
        'telefono',
        'direccion',
        'rfc',
        'razon_social',
        'codigo_postal',
        'regimen_fiscal',
    ];

    public function cotizaciones()
    {
        return $this->hasMany(Cotizacion::class, 'id_cliente');
    }

    public function ventas()
    {
        return $this->hasMany(Venta::class, 'id_cliente');
    }

    public function compras()
    {
        return $this->hasMany(Compra::class, 'id_cliente');
    }

    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'cotizacion_producto', 'cotizacion_id', 'id_producto')
                    ->withPivot('cantidad', 'precio');
    }

    public function tieneCotizaciones()
    {
        return $this->cotizaciones()->exists();
    }

    public function tieneVentas()
    {
        return $this->ventas()->exists();
    }

    public function puedeEliminar()
    {
        return !$this->tieneCotizaciones() && !$this->tieneVentas();
    }
}
