<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CotizacionProducto extends Pivot
{
    use HasFactory;

    protected $table = 'cotizacion_producto';

    protected $fillable = [
        'cotizacion_id',
        'producto_id',
        'cantidad',
        'precio',
    ];

    public function cotizacion()
    {
    }
    
    public function productos()
    {
        
    }

}
