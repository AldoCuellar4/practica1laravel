<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    // Especificar el nombre de la tabla si no sigue la convención de nombres plural
    protected $table = 'categorias';

    // Permitir la asignación masiva para los atributos
    protected $fillable = ['nombre'];

    // Define la relación con el modelo Producto
    public function productos()
    {
        return $this->hasMany(Producto::class, 'id_cat');
    }
}

