<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'id_producto';
    protected $table = 'productos';
    
    protected $fillable = [
        'nombre',
        'id_cat',
        'PV',
        'PC',
        'fecha_de_compra',
        'colores',
        'descripcion_corta',
        'descripcion_larga',
        'stock',
    ];
    
    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'id_cat');
    }
    
    public function compras()
    {
        return $this->hasMany(Compra::class, 'id_producto');
    }
    
    public function inventarios()
    {
        return $this->hasMany(Inventario::class, 'id_producto');
    }
    
    public function cotizaciones()
    {
        return $this->belongsToMany(Cotizacion::class, 'cotizacion_producto', 'producto_id', 'cotizacion_id')
                    ->withPivot('cantidad', 'precio');
    }
    
    public function ventas()
    {
        return $this->belongsToMany(Venta::class, 'venta_producto', 'producto_id', 'venta_id')
        ->withPivot('cantidad', 'precio')
        ->withTimestamps();
    }
}

