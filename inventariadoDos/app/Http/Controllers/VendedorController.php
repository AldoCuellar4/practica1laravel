<?php

namespace App\Http\Controllers;

use App\Models\Vendedor;
use Illuminate\Http\Request;

class VendedorController extends Controller
{
    public function index()
    {
        $vendedores = Vendedor::all();
        return view('vendedores.index', compact('vendedores'));
    }

    public function create()
    {
        return view('vendedores.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'correo' => 'required|string|email|max:255',
            'telefono' => 'required|string|max:20',
        ]);

        Vendedor::create($request->all());
        return redirect()->route('vendedores.index')->with('success', 'Vendedor creado exitosamente.');
    }

    public function show($id)
    {
        $vendedor = Vendedor::findOrFail($id);
        return view('vendedores.show', compact('vendedor'));
    }

    public function edit($id)
    {
        $vendedor = Vendedor::findOrFail($id);

        return view('vendedores.edit', compact('vendedor'));
    }

    public function update(Request $request, Vendedor $vendedor)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'correo' => 'required|string|email|max:255',
            'telefono' => 'required|string|max:20',
        ]);

        $vendedor->update($request->all());
        return redirect()->route('vendedores.index')->with('success', 'Vendedor actualizado exitosamente.');
    }

    public function destroy($id)
    {
        $vendedor = Vendedor::findOrFail($id);

        if (!$vendedor->puedeEliminar()) {
            return redirect()->route('vendedores.index')
                             ->with('error', 'No se puede eliminar el vendedor porque tiene ventas asociadas.');
        }

        $vendedor->delete();

        return redirect()->route('vendedores.index')
                        ->with('success', 'Vendedor eliminado exitosamente.');
    }
}
