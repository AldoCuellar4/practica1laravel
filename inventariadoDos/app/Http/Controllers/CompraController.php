<?php

namespace App\Http\Controllers;

use App\Models\Compra;
use App\Models\Producto;
use App\Models\Inventario;
use App\Models\Proveedor;
use Illuminate\Http\Request;

class CompraController extends Controller
{
    public function index()
    {
        $compras = Compra::with('producto', 'proveedor')->get();
        return view('compras.index', compact('compras'));
    }

    public function create()
    {
        $productos = Producto::all();
        $proveedores = Proveedor::all();
        return view('compras.create', compact('productos', 'proveedores'));
    }

    public function store(Request $request)
    {
        
        //dd($request->all());
        
        $request->validate([
            'proveedor' => 'required|exists:proveedores,id',
            'fecha_compra' => 'required|date',
            'productos' => 'required|array',
            'productos.*.id_producto' => 'required|exists:productos,id_producto',
            'productos.*.cantidad' => 'required|integer|min:1',
            'productos.*.precio' => 'required|numeric|min:0',
        ]);

        foreach ($request->productos as $productoData) {
            Compra::create([
                'id_producto' => $productoData['id_producto'],
                'id_proveedor' => $request->proveedor,
                'cantidad' => $productoData['cantidad'],
                'precio_unitario' => $productoData['precio'],
                'fecha_compra' => $request->fecha_compra,
            ]);

            // Actualizar el stock del producto
            $producto = Producto::find($productoData['id_producto']);
            $producto->stock += $productoData['cantidad'];
            $producto->save();

            Inventario::create([
                'id_producto' => $productoData['id_producto'],
                'id_cat' => $producto->id_cat,
                'fecha_entrada' => $request->fecha_compra,
                'movimiento' => 'entrada',
                'motivo' => 'compra',
                'cantidad' => $productoData['cantidad'],

            ]);
        }

        return redirect()->route('compras.index')->with('success', 'Compra creada con éxito');
    }

    public function show($id)
    {
        // Método show vacío
    }
    
    public function edit(Compra $compra)
    {
        $productos = Producto::all();
        $proveedores = Proveedor::all();
        return view('compras.edit', compact('compra', 'productos', 'proveedores'));
    }

    public function update(Request $request, Compra $compra)
    {
        $request->validate([
            'id_producto' => 'required|exists:productos,id_producto',
            'id_proveedor' => 'required|exists:proveedores,id',
            'cantidad' => 'required|integer|min:1',
            'precio_unitario' => 'required|numeric|min:0',
            'fecha_compra' => 'required|date',
        ]);




        // Actualizar el stock del producto
        $producto = Producto::find($compra->id_producto);
        $producto->stock -= $compra->cantidad;
        $producto->save();

        $compra->update($request->all());

        // Actualizar el stock del nuevo producto
        $producto = Producto::find($request->id_producto);
        $producto->stock += $request->cantidad;
        $producto->save();

        return redirect()->route('compras.index')->with('success', 'Compra actualizada con éxito');
    }

    public function destroy(Compra $compra)
    {
        // Actualizar el stock del producto
        $producto = Producto::find($compra->id_producto);
        $producto->stock -= $compra->cantidad;
        $producto->save();

        $compra->delete();

        return redirect()->route('compras.index')->with('success', 'Compra eliminada con éxito');
    }
}
