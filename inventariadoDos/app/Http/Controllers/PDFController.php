<?php

namespace App\Http\Controllers;

use App\Models\Venta;
use App\Models\Cotizacion;
use App\Models\Compra;
use App\Models\Inventario;
use App\Models\Producto;
use App\Models\Proveedor;
use App\Models\Cliente;
use App\Models\Vendedor;
use App\Models\FormaPago;


use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function exportarPDFVenta($id)
    {
        // Encuentra la venta con sus productos asociados
        $venta = Venta::with(['vendedor', 'cliente', 'productos'])->findOrFail($id);
        
        // Carga la vista y pasa la venta con productos al PDF
        $pdf = Pdf::loadView('pdf.ventas', compact('venta'));
        
        // Devuelve el PDF descargado con el nombre de archivo correspondiente
        return $pdf->download("venta_{$id}.pdf");
    }
    
    public function exportarPDFCotizacion($id)
    {
        // Encuentra la cotización con sus productos asociados
        $cotizacion = Cotizacion::with(['cliente', 'productos'])->findOrFail($id);
        
        // Carga la vista y pasa la cotización con productos al PDF
        $pdf = Pdf::loadView('pdf.cotizaciones', compact('cotizacion'));
        
        // Devuelve el PDF descargado con el nombre de archivo correspondiente
        return $pdf->download("cotizacion_{$id}.pdf");
    }
    
    public function exportarPDFCompras($param)
    {
        // Obtiene todas las compras con sus productos y proveedores asociados
        $compras = Compra::with(['producto', 'proveedor'])->get();
        
        // Puedes usar el parámetro para filtrar o hacer algo con él
        // Por ejemplo, filtrar por fecha, proveedor, etc.
        
        // Carga la vista y pasa las compras al PDF
        $pdf = Pdf::loadView('pdf.compras', compact('compras', 'param'));
        
        // Devuelve el PDF descargado con el nombre de archivo correspondiente
        return $pdf->download("compras_{$param}.pdf");
    }

    public function exportarPDFInventarios($param)
    {
        // Obtiene todos los inventarios con sus productos y categorías asociados
        $inventarios = Inventario::with(['producto', 'categoria'])->get();
        
        // Puedes usar el parámetro para filtrar o hacer algo con él
        // Por ejemplo, filtrar por fecha, categoría, etc.
        
        // Carga la vista y pasa los inventarios al PDF
        $pdf = Pdf::loadView('pdf.inventarios', compact('inventarios', 'param'));
        
        // Devuelve el PDF descargado con el nombre de archivo correspondiente
        return $pdf->download("inventarios_{$param}.pdf");
    }

    public function exportarPDFProductos()
    {
        // Cargar todos los productos con sus categorías
        $productos = Producto::with('categoria')->get();

        // Generar el PDF con todos los productos
        $pdf = Pdf::loadView('pdf.productos', compact('productos'));

        // Retornar el PDF descargable
        return $pdf->download('lista_productos.pdf');
    }

    public function exportarPDFProveedores()
    {
        $proveedores = Proveedor::all();

        $pdf = Pdf::loadView('pdf.proveedores', compact('proveedores'));

        return $pdf->download('listado_proveedores.pdf');
    }

    public function exportarPDFClientes()
    {
        $clientes = Cliente::all();

        $pdf = Pdf::loadView('pdf.clientes', compact('clientes'));

        return $pdf->download('listado_clientes.pdf');
    }

    public function exportarPDFVendedores()
    {
        $vendedores = Vendedor::all();

        // Carga la vista y pasa los vendedores al PDF
        $pdf = Pdf::loadView('pdf.vendedores', compact('vendedores'));

        // Retorna el PDF como un stream (visualización en el navegador)
        return $pdf->download('listado_vendedores.pdf');
    }

    public function exportarPDFFormasPago()
    {
        $formasPago = FormaPago::all();

        $pdf = Pdf::loadView('pdf.formas_pago', compact('formasPago'));

        return $pdf->download('listado_formas_pago.pdf');
    }

    public function exportarPDFstockPorProducto(){

        $productos = Producto::all();

        $pdf = Pdf::loadView('pdf.stockProducto',compact('productos'));

        return $pdf->download("stock_por_producto.pdf");
    }
    
}
