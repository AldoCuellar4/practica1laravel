<?php
namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Models\Inventario;

class ProductoController extends Controller
{
    public function index()
    {
        $productos = Producto::with('categoria')->get();
        return view('productos.index', compact('productos'));
    }

    public function create()
    {
        $categorias = Categoria::all();
        return view('productos.create', compact('categorias'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'id_cat' => 'required|exists:categorias,id',
            'PV' => 'required|numeric|min:0',
            'PC' => 'required|numeric|min:0',
            'fecha_de_compra' => 'required|date',
            'colores' => 'required|string|max:255',
            'descripcion_corta' => 'required|string|max:255',
            'descripcion_larga' => 'required|string',
        ]);

        Producto::create($request->all());

        return redirect()->route('productos.index')->with('success', 'Producto creado exitosamente.');
    }

    public function edit($id)
    {
        $producto = Producto::findOrFail($id);
        $categorias = Categoria::all();
        return view('productos.edit', compact('producto', 'categorias'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'id_cat' => 'required|exists:categorias,id',
            'PV' => 'required|numeric|min:0',
            'PC' => 'required|numeric|min:0',
            'fecha_de_compra' => 'required|date',
            'colores' => 'required|string|max:255',
            'descripcion_corta' => 'required|string|max:255',
            'descripcion_larga' => 'required|string',
        ]);

        $producto = Producto::findOrFail($id);
        $producto->update($request->all());

        return redirect()->route('productos.index')->with('success', 'Producto actualizado exitosamente.');
    }

    public function destroy($id_producto)
    {
        $producto = Producto::findOrFail($id_producto);

        // Verificar si existen registros en el inventario asociados al producto
        if ($producto->inventarios()->exists()) {
            return redirect()->route('productos.index')->with('error', 'No se puede eliminar el producto porque está asociado en el inventario.');
        }

        // Si no hay registros en el inventario asociados, proceder con la eliminación del producto
        $producto->delete();

        return redirect()->route('productos.index')->with('success', 'Producto eliminado exitosamente.');
    }

    public function search(Request $request)
    {
        $query = $request->input('query');
        $productos = Producto::where('nombre', 'LIKE', "%{$query}%")
                            ->orWhere('id_cat', 'LIKE', "%{$query}%") 
                            ->get();

        return response()->json($productos);
    }

    public function show($id)
    {
        $producto = Producto::with('categoria')->findOrFail($id);

        return view('productos.show', compact('producto'));
    }


}
