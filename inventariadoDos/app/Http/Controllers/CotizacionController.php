<?php

namespace App\Http\Controllers;

use App\Models\Cotizacion;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\CotizacionProducto;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Providers\FPDFWrapper;
class CotizacionController extends Controller
{
    
    
    public function index()
    {
        $cotizaciones = Cotizacion::with('cliente')->get();
        return view('cotizaciones.index', compact('cotizaciones'));
    }
    
    public function create()
    {
        $productos = Producto::all();
        $clientes = Cliente::all();
        return view('cotizaciones.create', compact('productos', 'clientes'));
    }
    
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'id_cliente' => 'required|exists:clientes,id',
            'fecha_cot' => 'required|date',
            'productos' => 'required|array',
            'productos.*.id_producto' => 'required|exists:productos,id_producto',
            'productos.*.cantidad' => 'required|integer|min:1',
            'productos.*.precio' => 'required|numeric|min:0',
        ]);
        
        
        // Crear la cotización
        $cotizacion = Cotizacion::create([
            'id_cliente' => $request->id_cliente,
            'fecha_cot' => $request->fecha_cot,
            'comentarios' => $request->comentarios,
            'total' => $request->total,
        ]);
        
        // Asociar los productos a la cotización
        foreach ($request->productos as $productoData) {
            CotizacionProducto::create([
                'cotizacion_id' => $cotizacion->id,
                'producto_id' => $productoData['id_producto'],
                'cantidad' => $productoData['cantidad'],
                'precio' => $productoData['precio'],
            ]);
        }
        
        return redirect()->route('cotizaciones.index')->with('success', 'Cotización creada con éxito');
    }
    
    
    public function show(Cotizacion $cotizacion)
    {
        $cotizacion->load('cliente', 'productos');
        
        if (!$cotizacion->cliente || $cotizacion->productos->isEmpty()) {
            Log::info('Datos incompletos:', [
                'cotizacion' => $cotizacion->toArray(),
                'cliente' => $cotizacion->cliente,
                'productos' => $cotizacion->productos
            ]);
            return response()->json(['message' => 'Datos de la cotización incompletos.'], 400);
        }
        
        return response()->json($cotizacion);
    }
    public function descargarPDF($id)
    {
        $cotizacion = Cotizacion::with('cliente', 'productos')->findOrFail($id);
        
        // Crear instancia de FPDFWrapper
        $pdf = new FPDFWrapper();
        $pdf->AddPage();
        
        // Configurar fuente
        $pdf->SetFont('Arial', 'B', 16);
        
        // Agregar título
        $pdf->Cell(0, 10, 'Cotizacion #' . $cotizacion->id, 0, 1, 'C');
        
        // Datos del cliente
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0, 10, 'Cliente: ' . $cotizacion->cliente->nombre, 0, 1);
        $pdf->Cell(0, 10, 'Fecha de Cotizacion: ' . \Carbon\Carbon::parse($cotizacion->fecha_cot)->format('d-m-Y'), 0, 1);
        $pdf->Cell(0, 10, 'Total: $' . number_format($cotizacion->total, 2), 0, 1);
        
        // Productos
        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(70, 10, 'Producto', 1);
        $pdf->Cell(30, 10, 'Cantidad', 1);
        $pdf->Cell(30, 10, 'Precio', 1);
        $pdf->Ln();
        
        $pdf->SetFont('Arial', '', 12);
        foreach ($cotizacion->productos as $producto) {
            $pdf->Cell(70, 10, $producto->nombre, 1);
            $pdf->Cell(30, 10, $producto->pivot->cantidad, 1);
            $pdf->Cell(30, 10, '$' . number_format($producto->pivot->precio, 2), 1);
            $pdf->Ln();
        }
        
        // Descargar el PDF
        $pdf->Output('D', 'cotizacion_' . $cotizacion->id . '.pdf');
        exit;
    }
    
    
    
    public function edit($id)
    {
        $cotizacion = Cotizacion::findOrFail($id);
        $productos = Producto::all();
        $clientes = Cliente::all();
        return view('cotizaciones.edit', compact('cotizacion', 'productos', 'clientes'));
    }
    
    public function update(Request $request, Cotizacion $cotizacion)
    {
        $request->validate([
            'id_cliente' => 'required|exists:clientes,id',
            'fecha_cot' => 'required|date',
            'productos' => 'required|array',
            'productos.*.id_producto' => 'required|exists:productos,id_producto',
            'productos.*.cantidad' => 'required|integer|min:1',
            'productos.*.precio' => 'required|numeric|min:0',
        ]);
        
        // Calcular el total de la cotización
        $total = array_reduce($request->productos, function ($carry, $producto) {
            return $carry + ($producto['cantidad'] * $producto['precio']);
        }, 0);
        
        // Actualizar la cotización
        $cotizacion->update([
            'id_cliente' => $request->id_cliente,
            'fecha_cot' => $request->fecha_cot,
            'comentarios' => $request->comentarios,
            'total' => $total,
        ]);
        
        // Actualizar los productos asociados a la cotización
        $cotizacion->productos()->detach();
        foreach ($request->productos as $productoData) {
            $cotizacion->productos()->attach($productoData['id_producto'], [
                'cantidad' => $productoData['cantidad'],
                'precio' => $productoData['precio']
            ]);
        }
        
        return redirect()->route('cotizaciones.index')->with('success', 'Cotización actualizada con éxito');
    }
    
    public function destroy($id)
    {
        $cotizacion = Cotizacion::findOrFail($id);
        
        // Eliminar los registros relacionados en la tabla pivote
        $cotizacion->productos()->delete();
        
        // Eliminar la cotización
        $cotizacion->delete();
        
        return redirect()->route('cotizaciones.index')->with('success', 'Cotización eliminada con éxito');
    }
    
}
