<?php
namespace App\Http\Controllers;

use App\Models\Venta;
use App\Models\Vendedor;
use App\Models\Producto;
use App\Models\Inventario;
use App\Models\Cliente;
use Illuminate\Http\Request;

class VentaController extends Controller
{
    public function index()
    {
        $ventas = Venta::with(['vendedor', 'cliente', 'productos'])->get();
        return view('ventas.index', compact('ventas'));
    }
    
    public function create()
    {
        $vendedores = Vendedor::all();
        $clientes = Cliente::all();
        $productos = Producto::all();
        return view('ventas.create', compact('vendedores', 'clientes', 'productos'));
    }

        public function store(Request $request)
        {
            #dd($request->all());
            
            // Validar los datos de entrada
            $validatedData = $request->validate([
                'id_vendedor' => 'required|exists:vendedores,id',
                'id_cliente' => 'required|exists:clientes,id',
                'monto' => 'required|numeric|min:0',
                'fecha_venta' => 'required|date',
                'productos.*.id_producto' => 'required|exists:productos,id_producto',
                'productos.*.cantidad' => 'required|integer|min:1',
                'productos.*.precio' => 'required|numeric|min:0',
            ]);
            
            
            // Crear la venta
            $venta = Venta::create([
                'id_vendedor' => $validatedData['id_vendedor'],
                'id_cliente' => $validatedData['id_cliente'],
                'monto' => $validatedData['monto'],
                'fecha_venta' => $validatedData['fecha_venta'],
            ]);
            
            // Asociar los productos a la venta
            foreach ($validatedData['productos'] as $producto) {
                $venta->productos()->attach($producto['id_producto'], [
                    'cantidad' => $producto['cantidad'],
                    'precio' => $producto['precio'],
                ]);
                $productoStock = Producto::find($producto['id_producto']);
                $productoStock->stock -= $producto['cantidad'];
                $productoStock->save();

                Inventario::create([
                    'id_producto' => $producto['id_producto'],
                    'id_cat' => $productoStock->id_cat,
                    'fecha_salida' => $request->fecha_venta,
                    'movimiento' => 'salida',
                    'motivo' => 'venta',
                    'cantidad' => $producto['cantidad'],
    
                ]);
            }
            
            // Redirigir a la lista de ventas con un mensaje de éxito
            return redirect()->route('ventas.index')->with('success', 'Venta creada exitosamente.');
        }
    
    
    
    public function edit($id)
    {
        $venta = Venta::findOrFail($id);
        $vendedores = Vendedor::all();
        $clientes = Cliente::all();
        return view('ventas.edit', compact('venta', 'vendedores', 'clientes'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_vendedor' => 'required|exists:vendedores,id',
            'id_cliente' => 'required|exists:clientes,id',
            'monto' => 'required|numeric',
            'fecha_venta' => 'required|date',
        ]);
        
        $venta = Venta::findOrFail($id);
        $venta->update($request->all());
        
        return redirect()->route('ventas.index')->with('success', 'Venta actualizada exitosamente.');
    }
    
    public function destroy(Venta $venta)
    {
        // Revertir el stock de los productos antes de eliminar la venta
        foreach ($venta->productos as $producto) {
            $productoStock = Producto::find($producto->pivot->id_producto);
            
            if ($productoStock) {
                // Incrementar el stock del producto
                $productoStock->stock += $producto->pivot->cantidad;
                $productoStock->save();

                // Registrar la entrada en el inventario
                Inventario::create([
                    'id_producto' => $producto->id_producto,
                    'id_cat' => $productoStock->id_cat,
                    'fecha_salida' => now(),
                    'movimiento' => 'entrada',
                    'motivo' => 'anulación de venta',
                    'cantidad' => $producto->pivot->cantidad,
                ]);
            }
        }

        // Eliminar las relaciones en la tabla pivot
        $venta->productos()->detach();

        // Eliminar la venta
        $venta->delete();

        return redirect()->route('ventas.index')->with('success', 'Venta eliminada exitosamente y stock revertido.');
    }
}
