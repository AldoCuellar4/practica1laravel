<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;

class CategoriaController extends Controller
{
    public function index()
    {
        $categorias = Categoria::all();
        return view('categorias.index', compact('categorias')); 
    }

    public function create()
    {
        return view('categorias.create');
    }
    

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:255',
        ]);

        Categoria::create($request->all());

        return redirect()->route('categorias.index')
                         ->with('success', 'Categoría creada con éxito.');
    }

    public function show($id)
    {
        $categoria = Categoria::find($id);
        return view('categorias.show', compact('categoria'));
    }

    public function edit($id)
    {
        $categoria = Categoria::find($id);
        return view('categorias.edit', compact('categoria'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|max:255',
        ]);

        $categoria = Categoria::find($id);
        $categoria->update($request->all());

        return redirect()->route('categorias.index')
                         ->with('success', 'Categoría actualizada con éxito.');
    }

    public function destroy($id)
    {
        $categoria = Categoria::find($id);

        // Verificar si la categoría está asociada a algún producto
        if ($categoria->productos()->exists()) {
            return redirect()->route('categorias.index')
                            ->with('error', 'No se puede eliminar la categoría porque está asociada a uno o más productos.');
        }

        $categoria->delete();

        return redirect()->route('categorias.index')
                        ->with('success', 'Categoría eliminada con éxito.');
    }

}
