<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormaPago;

class FormaDePagoController extends Controller
{
    public function index()
    {
        $formasDePago = FormaPago::all();
        return view('formas_de_pago.index', compact('formasDePago'));
    }

    public function create()
    {
        return view('formas_de_pago.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tipo' => 'required|string|max:255',
        ]);

        FormaPago::create([
            'tipo' => $request->tipo,
        ]);

        return redirect()->route('formas_de_pago.index')->with('success', 'Forma de pago creada correctamente.');
    }

    public function show($id)
    {
        $formaDePago = FormaPago::findOrFail($id);
        return view('formas_de_pago.show', compact('formaDePago'));
    }

    public function edit($id)
    {
        $formaDePago = FormaPago::findOrFail($id);
        return view('formas_de_pago.edit', compact('formaDePago'));
    }

    public function update(Request $request, FormaPago $formaDePago)
    {
        $request->validate([
            'tipo' => 'required|string|max:255',
        ]);

        $formaDePago->update([
            'tipo' => $request->tipo,
        ]);

        return redirect()->route('formas_de_pago.index')->with('success', 'Forma de pago actualizada correctamente.');
    }

    public function destroy(FormaPago $formaDePago)
    {
        $formaDePago->delete();

        return redirect()->route('formas_de_pago.index')->with('success', 'Forma de pago eliminada correctamente.');
    }
}
