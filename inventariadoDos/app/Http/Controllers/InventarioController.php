<?php

namespace App\Http\Controllers;

use App\Models\Inventario;
use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class InventarioController extends Controller
{
    public function index()
    {
        $inventarios = Inventario::with('producto', 'categoria')->get()->map(function ($inventario) {
            $inventario->fecha_entrada = Carbon::parse($inventario->fecha_entrada);
            if ($inventario->fecha_salida) {
                $inventario->fecha_salida = Carbon::parse($inventario->fecha_salida);
            }
            return $inventario;
        });

        return view('inventarios.index', compact('inventarios'));
    }

    public function create()
    {
        $productos = Producto::all();
        $categorias = Categoria::all();
        return view('inventarios.create', compact('productos', 'categorias'));
    }

    public function store(Request $request)
    {

        #dd($request->all());        
        // Validación de datos
        $request->validate([
            'id_producto' => 'required|integer',
            'fecha_entrada' => 'nullable|date',
            'fecha_salida' => 'nullable|date',
            'movimiento' => 'required|string',
            'motivo' => 'required|string',
            'cantidad' => 'required|integer',
        ]);
    
        // Preparar datos para la inserción
        $data = $request->all();
        $data['fecha_entrada'] = $request->fecha_entrada ? Carbon::parse($request->fecha_entrada) : null;
        $data['fecha_salida'] = $request->fecha_salida ? Carbon::parse($request->fecha_salida) : null;
    
        try {
            // Manejar transacciones y actualización de stock
            $inventario  = Inventario::create([
                'id_producto' => $data['id_producto'],
                'id_cat' => $request->input('id_cat'),
                'fecha_entrada' => $data['fecha_entrada'],
                'fecha_salida' => $data['fecha_salida'],
                'movimiento' => $data['movimiento'],
                'motivo' => $data['motivo'],
                'cantidad' => $data['cantidad'],
            ]);

            $this->updateProductStock($inventario->id_producto, $inventario->movimiento, $inventario->cantidad);
    
            return redirect()->route('inventarios.index')->with('success', 'Inventario creado con éxito.');
        } catch (\Exception $e) {
            // Manejar errores de inserción
            return redirect()->back()->withErrors(['error' => 'Error al crear el inventario: ' . $e->getMessage()]);
        }
    }


    private function updateProductStock($id_producto, $movimiento, $cantidad)
    {
        $producto = Producto::findOrFail($id_producto);

        if ($movimiento == '1') {
            $producto->stock += $cantidad;
        } else if ($movimiento == '2') {
            $producto->stock -= $cantidad;
        }

        $producto->save();
    }

    public function edit($id)
    {
        $inventario = Inventario::findOrFail($id);
        $inventario->fecha_entrada = Carbon::parse($inventario->fecha_entrada);
        if ($inventario->fecha_salida) {
            $inventario->fecha_salida = Carbon::parse($inventario->fecha_salida);
        }

        $productos = Producto::all();
        $categorias = Categoria::all();
        return view('inventarios.edit', compact('inventario', 'productos', 'categorias'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'id_producto' => 'required',
            'id_cat' => 'required',
            'fecha_entrada' => 'required|date',
            'fecha_salida' => 'nullable|date',
            'movimiento' => 'required|string',
            'motivo' => 'required|string',
            'cantidad' => 'required|integer',
        ]);

        $inventario = Inventario::findOrFail($id);
        $previousCantidad = $inventario->cantidad;
        $previousMovimiento = $inventario->movimiento;
        $data = $request->all();
        $data['fecha_entrada'] = Carbon::parse($request->fecha_entrada);
        if ($request->fecha_salida) {
            $data['fecha_salida'] = Carbon::parse($request->fecha_salida);
        }

        DB::transaction(function () use ($data, $inventario, $request, $previousCantidad, $previousMovimiento) {
            $inventario->update($data);

            // Revert the previous stock change
            $this->updateProductStock($request->id_producto, $previousMovimiento == '1' ? '2' : '1', $previousCantidad);

            // Apply the new stock change
            $this->updateProductStock($request->id_producto, $request->movimiento, $request->cantidad);
        });

        return redirect()->route('inventarios.index')->with('success', 'Inventario actualizado con éxito.');
    }
    public function show($id)
    {
        // Método show vacío
    }   

    public function destroy($id)
    {
        $inventario = Inventario::findOrFail($id);

        DB::transaction(function () use ($inventario) {
            if ($inventario->movimiento == '1') {
                $this->updateProductStock($inventario->id_producto, '2', $inventario->cantidad);
            } else if ($inventario->movimiento == '2') {
                $this->updateProductStock($inventario->id_producto, '1', $inventario->cantidad);
            }

            $inventario->delete();
        });

        return redirect()->route('inventarios.index')->with('success', 'Inventario eliminado con éxito.');
    }
}
