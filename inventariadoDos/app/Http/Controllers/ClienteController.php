<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        $clientes = Cliente::all();
        return view('clientes.index', compact('clientes'));
    }

    public function create()
    {
        return view('clientes.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'correo' => 'required|string|email|max:255|unique:clientes',
            'telefono' => 'required|string|max:20',
            'direccion' => 'required|string|max:255',
            'rfc' => 'required|string|max:13|unique:clientes',
            'razon_social' => 'required|string|max:255|unique:clientes',
            'codigo_postal' => 'required|string|max:255|unique:clientes',
            'regimen_fiscal' => 'required|string|max:255|unique:clientes',

        ]);

        Cliente::create($request->all());

        return redirect()->route('clientes.index')
                        ->with('success', 'Cliente creado exitosamente.');
    }

    public function show($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('clientes.show', compact('cliente'));
    }

    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('clientes.edit', compact('cliente'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'correo' => 'required|string|email|max:255|unique:clientes,correo,'.$id,
            'telefono' => 'required|string|max:20',
            'direccion' => 'required|string|max:255',
            'rfc' => 'required|string|max:13|unique:clientes,rfc,'.$id,
        ]);

        $cliente = Cliente::findOrFail($id);
        $cliente->update($request->all());

        return redirect()->route('clientes.index')
                        ->with('success', 'Cliente actualizado exitosamente.');
    }

    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);

        if (!$cliente->puedeEliminar()) {
            return redirect()->route('clientes.index')
                             ->with('error', 'No se puede eliminar el cliente porque tiene cotizaciones o ventas asociadas.');
        }

        $cliente->delete();

        return redirect()->route('clientes.index')
                        ->with('success', 'Cliente eliminado exitosamente.');
    }
}
