<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id_producto');
            $table->string('nombre');
            $table->unsignedBigInteger('id_cat');
            $table->decimal('PV', 8, 2);
            $table->decimal('PC', 8, 2);
            $table->date('fecha_de_compra');
            $table->string('colores');
            $table->string('descripcion_corta');
            $table->text('descripcion_larga');
            $table->foreign('id_cat')->references('id')->on('categorias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('productos');
    }
};
