<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->bigIncrements('id');  // Se asegura de que el campo 'id' es el identificador principal
            $table->unsignedBigInteger('id_cliente');
            $table->decimal('total', 8, 2);
            $table->date('fecha_cot');
            $table->string('comentarios')->nullable();
            $table->timestamps();

            $table->foreign('id_cliente')->references('id')->on('clientes')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('cotizaciones');
    }
};
