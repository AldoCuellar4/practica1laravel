<?php
use App\Http\Controllers\PDFController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ProveedorController;
use App\Http\Controllers\InventarioController;
use App\Http\Controllers\CompraController;
use App\Http\Controllers\CotizacionController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\FormaDePagoController;
use App\Http\Controllers\VendedorController;
use App\Http\Controllers\VentaController;

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/productos',function(){
    return view('productos');
})->middleware(['auth','verified'])->name('productos');


//Rutas para las categorias
Route::resource('categorias', CategoriaController::class);
Route::get('categorias/buscar/{nombre}', [CategoriaController::class, 'buscar'])->name('categorias.buscar');


//Ruta para los productos
Route::resource('productos', ProductoController::class);



//Ruta para los proveedores
Route::resource('proveedores', ProveedorController::class);


//Ruta para Inventarios
Route::resource('inventarios', InventarioController::class);


//Ruta para compras
Route::resource('compras',CompraController::class);

//Ruta para cotizaciones
Route::resource('cotizaciones',CotizacionController::class);

//Ruta para clientes
Route::resource('clientes', ClienteController::class);

//Ruta para formas de pago
Route::resource('formas_de_pago', FormaDePagoController::class);

//Ruta para vendedores
Route::resource('vendedores', VendedorController::class);

Route::get('/api/productos', [ProductoController::class, 'search'])->name('productos.search');


//Ruta para compras
Route::resource('compras', CompraController::class);


//Ruta para ventas
Route::resource('ventas', VentaController::class);

//PDFS
Route::get('ventas/pdf/{id}', [PDFController::class, 'exportarPDFVenta'])->name('ventas.pdf');
Route::get('cotizaciones/pdf/{id}', [PDFController::class, 'exportarPDFCotizacion'])->name('cotizaciones.pdf');
Route::get('compras/pdf/{param}', [PDFController::class, 'exportarPDFCompras'])->name('compras.pdf');
Route::get('inventarios/pdf/{param}', [PDFController::class, 'exportarPDFInventarios'])->name('inventarios.pdf');
Route::get('producto-pdf', [PDFController::class, 'exportarPDFProductos'])->name('productos.pdf');
Route::get('stock_producto-pdf', [PDFController::class, 'exportarPDFstockPorProducto'])->name('stock_por_producto.pdf');
Route::get('proveedores-pdf', [PDFController::class, 'exportarPDFProveedores'])->name('proveedores.pdf');
Route::get('clientes-pdf', [PDFController::class, 'exportarPDFClientes'])->name('clientes.pdf');
Route::get('vendedores-pdf', [PDFController::class, 'exportarPDFVendedores'])->name('vendedores.pdf');
Route::get('formas-pago-pdf', [PDFController::class, 'exportarPDFFormasPago'])->name('formas_pago.pdf');



Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
