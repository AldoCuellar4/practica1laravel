@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-center mt-8">
        <div class="w-full max-w-2xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-2xl font-semibold">{{ __('Crear Nueva Categoría') }}</h2>
                </div>

                <div class="p-6">
                    <form action="{{ route('categorias.store') }}" method="POST">
                        @csrf
                        <div class="mb-6">
                            <label for="nombre" class="block text-gray-700 text-sm font-semibold mb-2">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        </div>
                        <div class="flex justify-end space-x-4">
                            <a href="{{ route('categorias.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                            <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg inline-block hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

