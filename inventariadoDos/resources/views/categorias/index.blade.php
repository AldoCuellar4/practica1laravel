@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Listado de Categorías</h2>
        <a href="{{ route('categorias.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Crear Nueva Categoría</a>
    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        @if (session('success'))
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-6" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mb-6" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Nombre</th>
                        <th class="px-6 py-3 border text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categorias as $categoria)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $categoria->id }}</td>
                        <td class="px-6 py-4 border">{{ $categoria->nombre }}</td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('categorias.show', $categoria->id) }}" class="bg-blue-500 text-white px-4 py-1 rounded-lg text-sm mr-2 hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Ver</a>
                            <a href="{{ route('categorias.edit', $categoria->id) }}" class="bg-yellow-500 text-white px-4 py-1 rounded-lg text-sm mr-2 hover:bg-yellow-600 focus:outline-none focus:ring focus:ring-yellow-300 transition duration-200">Editar</a>
                            <form action="{{ route('categorias.destroy', $categoria->id) }}" method="POST" class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="bg-red-500 text-white px-4 py-1 rounded-lg text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200" onclick="return confirm('¿Estás seguro de eliminar esta categoría?')">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
