@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-center">
        <div class="w-full max-w-2xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-2xl font-semibold">{{ __('Detalles de la Categoría') }}</h2>
                </div>
                
                <div class="p-6">
                    <div class="mb-4">
                        <strong class="block text-gray-700 text-sm font-semibold">ID:</strong>
                        <span class="text-gray-900">{{ $categoria->id }}</span>
                    </div>
                    <div class="mb-4">
                        <strong class="block text-gray-700 text-sm font-semibold">Nombre:</strong>
                        <span class="text-gray-900">{{ $categoria->nombre }}</span>
                    </div>
                    <div class="flex justify-end mt-4">
                        <a href="{{ route('categorias.index') }}" class="bg-blue-500 text-white px-4 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300">Volver</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
