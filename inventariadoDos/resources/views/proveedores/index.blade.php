@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Lista de Proveedores</h2>
        <a href="{{ route('proveedores.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Crear Proveedor</a>
        <a href="{{ route('proveedores.pdf') }}" class="bg-green-500 text-white px-6 py-2 rounded-lg hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Exportar PDF</a>

    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Nombre de Contacto</th>
                        <th class="px-6 py-3 border">Correo</th>
                        <th class="px-6 py-3 border">Teléfono</th>
                        <th class="px-6 py-3 border">Dirección</th>
                        <th class="px-6 py-3 border text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($proveedores as $proveedor)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $proveedor->id }}</td>
                        <td class="px-6 py-4 border">{{ $proveedor->nombre_contacto }}</td>
                        <td class="px-6 py-4 border">{{ $proveedor->correo }}</td>
                        <td class="px-6 py-4 border">{{ $proveedor->telefono }}</td>
                        <td class="px-6 py-4 border">{{ $proveedor->direccion }}</td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('proveedores.edit', $proveedor->id) }}" class="bg-yellow-500 text-white px-4 py-2 rounded-full text-sm mr-2 hover:bg-yellow-600 focus:outline-none focus:ring focus:ring-yellow-300 transition duration-200">Editar</a>
                            @if ($proveedor->compras()->count() > 0)
                                <button disabled class="bg-gray-500 text-white px-4 py-2 rounded-full text-sm cursor-not-allowed" title="No se puede eliminar porque está asociado a una compra">Eliminar</button>
                            @else
                                <form action="{{ route('proveedores.destroy', $proveedor->id) }}" method="POST" class="inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="bg-red-500 text-white px-4 py-2 rounded-full text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200">Eliminar</button>
                                </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
