@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <div class="flex justify-center mt-8">
        <div class="w-full max-w-2xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-2xl font-semibold">{{ __('Editar Proveedor') }}</h2>
                </div>

                <div class="p-6">
                    <form action="{{ route('proveedores.update', $proveedor->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-6">
                            <label for="nombre_contacto" class="block text-gray-700 text-sm font-semibold mb-2">Nombre de Contacto:</label>
                            <input type="text" id="nombre_contacto" name="nombre_contacto" value="{{ $proveedor->nombre_contacto }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        </div>
                        <div class="mb-6">
                            <label for="correo" class="block text-gray-700 text-sm font-semibold mb-2">Correo:</label>
                            <input type="email" id="correo" name="correo" value="{{ $proveedor->correo }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        </div>
                        <div class="mb-6">
                            <label for="telefono" class="block text-gray-700 text-sm font-semibold mb-2">Teléfono:</label>
                            <input type="text" id="telefono" name="telefono" value="{{ $proveedor->telefono }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        </div>
                        <div class="mb-6">
                            <label for="direccion" class="block text-gray-700 text-sm font-semibold mb-2">Dirección:</label>
                            <input type="text" id="direccion" name="direccion" value="{{ $proveedor->direccion }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        </div>
                        <div class="flex justify-end space-x-4">
                            <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Guardar</button>
                            <a href="{{ route('proveedores.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
