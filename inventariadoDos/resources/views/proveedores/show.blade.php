@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4">
    <div class="flex justify-center mt-8">
        <div class="w-full max-w-2xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-blue-800 text-white px-6 py-4">
                    <h2 class="text-2xl font-semibold">{{ __('Detalles del Proveedor') }}</h2>
                </div>

                <div class="p-6">
                    <div class="mb-4">
                        <strong>ID:</strong> {{ $proveedor->id }}
                    </div>
                    <div class="mb-4">
                        <strong>Nombre de Contacto:</strong> {{ $proveedor->nombre_contacto }}
                    </div>
                    <div class="mb-4">
                        <strong>Correo:</strong> {{ $proveedor->correo }}
                    </div>
                    <div class="mb-4">
                        <strong>Teléfono:</strong> {{ $proveedor->telefono }}
                    </div>
                    <div class="mb-4">
                        <strong>Dirección:</strong> {{ $proveedor->direccion }}
                    </div>
                    <a href="{{ route('proveedores.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
