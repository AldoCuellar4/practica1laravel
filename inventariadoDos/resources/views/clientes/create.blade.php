@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-center mt-8">
        <div class="w-full max-w-2xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-2xl font-semibold">Crear Nuevo Cliente</h2>
                </div>

                <div class="p-6">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('clientes.store') }}" method="POST">
                        @csrf
                        <div class="mb-6">
                            <label for="nombre" class="block text-gray-700 text-sm font-semibold mb-2">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('nombre') }}" required>
                        </div>
                        <div class="mb-6">
                            <label for="correo" class="block text-gray-700 text-sm font-semibold mb-2">Correo:</label>
                            <input type="email" id="correo" name="correo" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('correo') }}" required>
                        </div>
                        <div class="mb-6">
                            <label for="telefono" class="block text-gray-700 text-sm font-semibold mb-2">Teléfono:</label>
                            <input type="tel" id="telefono" name="telefono" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('telefono') }}" required>
                        </div>
                        <div class="mb-6">
                            <label for="direccion" class="block text-gray-700 text-sm font-semibold mb-2">Dirección:</label>
                            <input type="text" id="direccion" name="direccion" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('direccion') }}" required>
                        </div>
                        <div class="mb-6">
                            <label for="rfc" class="block text-gray-700 text-sm font-semibold mb-2">RFC:</label>
                            <input type="text" id="rfc" name="rfc" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('rfc') }}" required>
                        </div>
                        <div class="mb-6">
                            <label for="razon_social" class="block text-gray-700 text-sm font-semibold mb-2">Razón Social:</label>
                            <input type="text" id="razon_social" name="razon_social" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('codigo_postal') }}" required>
                        </div>
                        <div class="mb-6">
                            <label for="codigo_postal" class="block text-gray-700 text-sm font-semibold mb-2">Código Postal:</label>
                            <input type="text" id="codigo_postal" name="codigo_postal" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('codigo_postal') }}" required>
                        </div>
                        <div class="mb-6">
                            <label for="regimen_fiscal" class="block text-gray-700 text-sm font-semibold mb-2">Régimen Fiscal:</label>
                            <input type="text" id="regimen_fiscal" name="regimen_fiscal" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('regimen_fiscal') }}" required>
                        </div>
                        <div class="flex justify-end space-x-4">
                            <a href="{{ route('clientes.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                            <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg inline-block hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Guardar Cliente</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
