@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Listado de Clientes</h2>
        <a href="{{ route('clientes.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Crear Nuevo Cliente</a>
        <a href="{{ route('clientes.pdf') }}" class="bg-green-500 text-white px-6 py-2 rounded-lg hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Exportar PDF</a>

    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        @if (session('success'))
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-6" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mb-6" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Nombre</th>
                        <th class="px-6 py-3 border">Correo</th>
                        <th class="px-6 py-3 border">Teléfono</th>
                        <th class="px-6 py-3 border">Dirección</th>
                        <th class="px-6 py-3 border">RFC</th>
                        <th class="px-6 py-3 border text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $cliente)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $cliente->id }}</td>
                        <td class="px-6 py-4 border">{{ $cliente->nombre }}</td>
                        <td class="px-6 py-4 border">{{ $cliente->correo }}</td>
                        <td class="px-6 py-4 border">{{ $cliente->telefono }}</td>
                        <td class="px-6 py-4 border">{{ $cliente->direccion }}</td>
                        <td class="px-6 py-4 border">{{ $cliente->rfc }}</td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('clientes.edit', $cliente->id) }}" class="bg-yellow-500 text-white px-4 py-1 rounded-lg text-sm mr-2 hover:bg-yellow-600 focus:outline-none focus:ring focus:ring-yellow-300 transition duration-200">Editar</a>
                            
                            @if($cliente->tieneVentas())
                                <button type="button" class="bg-gray-500 text-white px-4 py-1 rounded-lg text-sm hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200" disabled>No se puede eliminar</button>
                            @else
                                <form action="{{ route('clientes.destroy', $cliente->id) }}" method="POST" class="inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="bg-red-500 text-white px-4 py-1 rounded-lg text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200" onclick="return confirm('¿Estás seguro de eliminar este cliente?')">Eliminar</button>
                                </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection