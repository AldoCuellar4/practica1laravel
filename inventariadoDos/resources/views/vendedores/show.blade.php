<!-- resources/views/vendedores/show.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="px-6 py-4">
            <h1 class="text-2xl font-semibold mb-4">Detalle de Vendedor</h1>
            <div class="mb-4">
                <strong>Nombre:</strong> {{ $vendedor->nombre }}
            </div>
            <div class="mb-4">
                <strong>Correo:</strong> {{ $vendedor->correo }}
            </div>
            <div class="mb-4">
                <strong>Teléfono:</strong> {{ $vendedor->telefono }}
            </div>
            <div>
                <a href="{{ route('vendedores.index') }}" class="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600">Volver</a>
            </div>
        </div>
    </div>
</div>
@endsection
