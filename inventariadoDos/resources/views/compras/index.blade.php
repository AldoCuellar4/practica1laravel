@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">{{ __('Compras') }}</h2>
        <div>
            <a href="{{ route('compras.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Agregar Compra</a>
            <a href="{{ route('compras.pdf', ['param' => '2024']) }}" class="bg-green-500 text-white px-6 py-2 rounded-lg hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Descargar PDF</a>
        </div>
    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">Producto</th>
                        <th class="px-6 py-3 border">Proveedor</th>
                        <th class="px-6 py-3 border">Cantidad</th>
                        <th class="px-6 py-3 border">Precio Unitario</th>
                        <th class="px-6 py-3 border">Fecha de Compra</th>
                        <th class="px-6 py-3 border text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($compras as $compra)
                        <tr class="bg-white">
                            <td class="px-6 py-4 border">{{ $compra->producto->nombre }}</td>
                            <td class="px-6 py-4 border">{{ $compra->proveedor->nombre_contacto }}</td>
                            <td class="px-6 py-4 border text-center">{{ $compra->cantidad }}</td>
                            <td class="px-6 py-4 border text-center">{{ $compra->precio_unitario }}</td>
                            <td class="px-6 py-4 border">{{ \Carbon\Carbon::parse($compra->fecha_compra)->format('d-m-Y') }}</td>
                            <td class="px-6 py-4 border text-center">
                                <form action="{{ route('compras.destroy', $compra->id) }}" method="POST" class="inline-block" onsubmit="return confirm('¿Estás seguro de eliminar esta compra?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="bg-red-500 text-white px-4 py-2 rounded-full text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
