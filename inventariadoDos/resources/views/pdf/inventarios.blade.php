<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventarios PDF</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        table, th, td {
            border: 1px solid black;
        }
        th, td {
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h2>Lista de Inventarios</h2>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Producto</th>
                <th>Categoría</th>
                <th>Fecha de Entrada</th>
                <th>Fecha de Salida</th>
                <th>Movimiento</th>
                <th>Motivo</th>
                <th>Cantidad</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($inventarios as $inventario)
            <tr>
                <td>{{ $inventario->id }}</td>
                <td>{{ $inventario->producto->nombre }}</td>
                <td>{{ $inventario->categoria->nombre }}</td>
                <td>{{ $inventario->fecha_entrada ? $inventario->fecha_entrada : '' }}</td>
                <td>{{ $inventario->fecha_salida ? $inventario->fecha_salida : 'N/A' }}</td>
                <td>{{ $inventario->movimiento }}</td>
                <td>{{ $inventario->motivo }}</td>
                <td>{{ $inventario->cantidad }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
