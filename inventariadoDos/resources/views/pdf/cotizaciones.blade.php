<!DOCTYPE html>
<html>
<head>
    <title>Cotización {{ $cotizacion->id }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h1>Reporte de Cotización #{{ $cotizacion->id }}</h1>
    <p><strong>Cliente:</strong> {{ $cotizacion->cliente->nombre }}</p>
    <p><strong>Fecha de Cotización:</strong> {{ \Carbon\Carbon::parse($cotizacion->fecha_cot)->format('d-m-Y') }}</p>
    <p><strong>Total:</strong> {{ number_format($cotizacion->total, 2) }}</p>
    <p><strong>Comentarios:</strong> {{ $cotizacion->comentarios }}</p>
    
    <h2>Productos</h2>
    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Precio</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cotizacion->productos as $producto)
            <tr>
                <td>{{ $producto->nombre }}</td>
                <td>{{ $producto->pivot->cantidad }}</td>
                <td>{{ number_format($producto->pivot->precio, 2) }}</td>
                <td>{{ number_format($producto->pivot->cantidad * $producto->pivot->precio, 2) }}</td>
            </tr>
            @empty
            <tr>
                <td colspan="4">No hay productos en esta cotización.</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</body>
</html>
