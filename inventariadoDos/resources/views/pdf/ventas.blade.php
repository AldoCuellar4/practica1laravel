<!DOCTYPE html>
<html>
<head>
    <title>Venta {{ $venta->id }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h1>Reporte de Venta #{{ $venta->id }}</h1>
    <p><strong>Vendedor:</strong> {{ $venta->vendedor->nombre }}</p>
    <p><strong>Cliente:</strong> {{ $venta->cliente->nombre }}</p>
    <p><strong>Monto:</strong> {{ number_format($venta->monto, 2) }}</p>
    <p><strong>Fecha de Venta:</strong> {{ \Carbon\Carbon::parse($venta->fecha_venta)->format('d-m-Y') }}</p>
    
    <h2>Productos</h2>
    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Precio</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($venta->productos as $producto)
            <tr>
                <td>{{ $producto->nombre }}</td>
                <td>{{ $producto->pivot->cantidad }}</td>
                <td>{{ number_format($producto->pivot->precio, 2) }}</td>
                <td>{{ number_format($producto->pivot->cantidad * $producto->pivot->precio, 2) }}</td>
            </tr>
            @empty
            <tr>
                <td colspan="4">No hay productos en esta venta.</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</body>
</html>
