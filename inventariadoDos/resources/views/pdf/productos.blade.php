<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Productos</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h1>Lista de Productos</h1>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Stock</th>
                <th>Categoria</th>
                <th>Precio de Venta</th>
                <th>Precio de Compra</th>
                <th>Fecha de Compra</th>
                <th>Colores</th>
            </tr>
        </thead>
        <tbody>
            @foreach($productos as $producto)
                <tr>
                    <td>{{ $producto->id_producto }}</td>
                    <td>{{ $producto->nombre }}</td>
                    <td>{{ $producto->stock }}</td>
                    <td>{{ $producto->categoria->nombre ?? 'No disponible' }}</td>
                    <td>{{ number_format($producto->PV, 2) }}</td>
                    <td>{{ number_format($producto->PC, 2) }}</td>
                    <td>{{ $producto->fecha_de_compra }}</td>
                    <td>{{ $producto->colores }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
