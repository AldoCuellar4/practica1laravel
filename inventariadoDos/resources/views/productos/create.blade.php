@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-center">
        <div class="w-full max-w-4xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-xl font-semibold">{{ __('Crear Nuevo Producto') }}</h2>
                </div>
                <div class="p-6">
                    <form action="{{ route('productos.store') }}" method="POST" id="producto-form">
                        @csrf
                        <div class="mb-4">
                            <label for="nombre" class="block text-gray-700">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="id_cat" class="block text-gray-700">Categoría:</label>
                            <select id="id_cat" name="id_cat" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                                @foreach ($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-4">
                            <label for="PV" class="block text-gray-700">Precio de Venta:</label>
                            <input type="number" id="PV" name="PV" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                            <p id="PV-error" class="text-red-500 text-sm mt-2" style="display: none;">El precio de venta no puede exceder los 500,000.</p>
                        </div>
                        <div class="mb-4">
                            <label for="PC" class="block text-gray-700">Precio de Compra:</label>
                            <input type="number" id="PC" name="PC" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                            <p id="PC-error" class="text-red-500 text-sm mt-2" style="display: none;">El precio de compra no puede exceder los 500,000.</p>
                        </div>
                        <div class="mb-4">
                            <label for="fecha_de_compra" class="block text-gray-700">Fecha de Compra:</label>
                            <input type="date" id="fecha_de_compra" name="fecha_de_compra" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300">
                        </div>
                        <div class="mb-4">
                            <label for="colores" class="block text-gray-700">Colores:</label>
                            <input type="text" id="colores" name="colores" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="descripcion_corta" class="block text-gray-700">Descripción Corta:</label>
                            <input type="text" id="descripcion_corta" name="descripcion_corta" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="descripcion_larga" class="block text-gray-700">Descripción Larga:</label>
                            <textarea id="descripcion_larga" name="descripcion_larga" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required></textarea>
                        </div>
                        <div class="flex justify-between">
                            <a href="{{ route('productos.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                            <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var formulario = document.getElementById('producto-form');
        var precioVentaInput = document.getElementById('PV');
        var precioCompraInput = document.getElementById('PC');
        var precioVentaError = document.getElementById('PV-error');
        var precioCompraError = document.getElementById('PC-error');

        function validateInput(input, errorElement) {
            if (parseFloat(input.value) > 500000) {
                errorElement.style.display = 'block';
            } else {
                errorElement.style.display = 'none';
            }
        }

        precioVentaInput.addEventListener('input', function() {
            validateInput(precioVentaInput, precioVentaError);
        });

        precioCompraInput.addEventListener('input', function() {
            validateInput(precioCompraInput, precioCompraError);
        });

        formulario.addEventListener('submit', function(e) {
            var precioVenta = parseFloat(precioVentaInput.value);
            var precioCompra = parseFloat(precioCompraInput.value);
            
            if (precioVenta > 500000 || precioCompra > 500000) {
                e.preventDefault();
                if (precioVenta > 500000) {
                    precioVentaError.style.display = 'block';
                }
                if (precioCompra > 500000) {
                    precioCompraError.style.display = 'block';
                }
            }
        });
    });
</script>
@endsection
