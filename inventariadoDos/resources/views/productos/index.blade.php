@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Listado de Productos</h2>
        <a href="{{ route('productos.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Crear Nuevo Producto</a>
        <a href="{{ route('productos.pdf') }}" class="bg-green-500 text-white px-6 py-2 rounded-lg hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Exportar listado</a>
        <a href="{{ route('stock_por_producto.pdf') }}" class="bg-green-500 text-white px-6 py-2 rounded-lg hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Exportar stock</a>

    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        @if (session('error'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mb-6" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Nombre</th>
                        <th class="px-6 py-3 border">Categoría</th>
                        <th class="px-6 py-3 border">PV</th>
                        <th class="px-6 py-3 border">PC</th>
                        <th class="px-6 py-3 border">Stock</th>
                        <th class="px-6 py-3 border">Fecha de Compra</th>
                        <th class="px-6 py-3 border text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($productos as $producto)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $producto->id_producto }}</td>
                        <td class="px-6 py-4 border">{{ $producto->nombre }}</td>
                        <td class="px-6 py-4 border">{{ $producto->categoria->nombre }}</td>
                        <td class="px-6 py-4 border">{{ $producto->PV }}</td>
                        <td class="px-6 py-4 border">{{ $producto->PC }}</td>
                        <td class="px-6 py-4 border">{{ $producto->stock }}</td>

                        <td class="px-6 py-4 border text-center">
                            @if ($producto->fecha_de_compra)
                                {{ \Carbon\Carbon::parse($producto->fecha_de_compra)->format('d-m-Y') }}
                            @else
                                {{ 'N/A' }}
                            @endif
                        </td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('productos.edit', $producto->id_producto) }}" class="bg-yellow-500 text-white px-4 py-2 rounded-lg text-sm mr-2 hover:bg-yellow-600 focus:outline-none focus:ring focus:ring-yellow-300 transition duration-200">Editar</a>
                            <form action="{{ route('productos.destroy', $producto->id_producto) }}" method="POST" class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="bg-red-500 text-white px-4 py-2 rounded-lg text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200" onclick="return confirm('¿Estás seguro de eliminar este producto?')">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
