@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-center">
        <div class="w-full max-w-4xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-xl font-semibold">{{ __('Editar Producto') }}</h2>
                </div>
                <div class="p-6">
                    <form action="{{ route('productos.update', $producto->id_producto) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-4">
                            <label for="nombre" class="block text-gray-700">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" value="{{ $producto->nombre }}" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="id_cat" class="block text-gray-700">Categoría:</label>
                            <select id="id_cat" name="id_cat" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                                @foreach ($categorias as $categoria)
                                    <option value="{{ $categoria->id }}" {{ $producto->id_cat == $categoria->id ? 'selected' : '' }}>{{ $categoria->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-4">
                            <label for="PV" class="block text-gray-700">Precio de Venta:</label>
                            <input type="number" id="PV" name="PV" value="{{ $producto->PV }}" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="PC" class="block text-gray-700">Precio de Compra:</label>
                            <input type="number" id="PC" name="PC" value="{{ $producto->PC }}" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="fecha_de_compra" class="block text-gray-700">Fecha de Compra:</label>
                            <input type="date" id="fecha_de_compra" name="fecha_de_compra" value="{{ $producto->fecha_de_compra }}" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300">
                        </div>
                        <div class="mb-4">
                            <label for="colores" class="block text-gray-700">Colores:</label>
                            <input type="text" id="colores" name="colores" value="{{ $producto->colores }}" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="descripcion_corta" class="block text-gray-700">Descripción Corta:</label>
                            <input type="text" id="descripcion_corta" name="descripcion_corta" value="{{ $producto->descripcion_corta }}" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>
                        </div>
                        <div class="mb-4">
                            <label for="descripcion_larga" class="block text-gray-700">Descripción Larga:</label>
                            <textarea id="descripcion_larga" name="descripcion_larga" class="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300" required>{{ $producto->descripcion_larga }}</textarea>
                        </div>
                        <div class="flex justify-between">
                            <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Actualizar</button>
                            <a href="{{ route('productos.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
