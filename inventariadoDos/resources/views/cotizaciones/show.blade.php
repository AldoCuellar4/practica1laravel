@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <h1 class="text-2xl font-semibold mb-6">Detalle de la Cotización</h1>

    <div class="bg-white shadow-md rounded-lg p-6 mb-4">
        <h2 class="text-xl font-semibold mb-4">Información del Cliente</h2>
        @if ($cotizacion->cliente)
            <p><strong>Nombre:</strong> {{ $cotizacion->cliente->nombre }}</p>
        @else
            <p><strong>Nombre:</strong> Cliente no disponible</p>
        @endif
        <p><strong>Fecha de Cotización:</strong> {{ \Carbon\Carbon::parse($cotizacion->fecha_cot)->format('d-m-Y') }}</p>
        <p><strong>Total:</strong> {{ number_format($cotizacion->total, 2) }}</p>
        <p><strong>Comentarios:</strong> {{ $cotizacion->comentarios }}</p>
    </div>

    <div class="bg-white shadow-md rounded-lg p-6">
        <h2 class="text-xl font-semibold mb-4">Productos</h2>
        <table class="min-w-full bg-white border border-gray-300">
            <thead>
                <tr class="bg-gray-100 text-gray-700">
                    <th class="px-6 py-3 border">Producto</th>
                    <th class="px-6 py-3 border">Cantidad</th>
                    <th class="px-6 py-3 border">Precio Unitario</th>
                    <th class="px-6 py-3 border">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cotizacion->productos as $producto)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border">{{ $producto->nombre }}</td>
                        <td class="px-6 py-4 border text-center">{{ $producto->pivot->cantidad }}</td>
                        <td class="px-6 py-4 border text-center">{{ number_format($producto->pivot->precio, 2) }}</td>
                        <td class="px-6 py-4 border text-center">{{ number_format($producto->pivot->cantidad * $producto->pivot->precio, 2) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
