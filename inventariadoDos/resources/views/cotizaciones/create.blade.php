@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="px-6 py-4">
            <h2 class="text-2xl font-semibold text-gray-800 mb-6">{{ __('Nueva Cotizacion') }}</h2>
            
            <form action="{{ route('cotizaciones.store') }}" method="POST" id="cotizacion-form">
                @csrf
                <div class="grid grid-cols-1 md:grid-cols-2 gap-6">
                    <!-- Productos -->
                    <div>
                        <h3 class="text-lg font-semibold text-gray-800 mb-4">{{ __('Productos') }}</h3>
                        <div class="mb-4">
                            <input type="text" id="buscar-producto" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200" placeholder="Buscar por nombre, categoría, SKU o código de barras">
                            <div id="resultados-busqueda" class="mt-2"></div>
                        </div>
                        <div class="mt-4">
                            <table class="w-full text-left table-auto">
                                <thead>
                                    <tr>
                                        <th class="px-4 py-2">{{ __('Nombre') }}</th>
                                        <th class="px-4 py-2">{{ __('Cantidad') }}</th>
                                        <th class="px-4 py-2">{{ __('Precio') }}</th>
                                        <th class="px-4 py-2">{{ __('Acción') }}</th>
                                    </tr>
                                </thead>
                                <tbody id="productos-seleccionados">
                                    <!-- Las filas de productos seleccionados se añadirán aquí -->
                                </tbody>
                            </table>
                        </div>
                        <div class="flex flex-col sm:flex-row justify-between items-start sm:items-center mt-6">
                            <span class="text-gray-600">{{ __('Subtotal:') }} <span id="subtotal" class="font-semibold">$0.00</span></span>
                            <span class="text-gray-600">{{ __('Descuento:') }} <span id="descuento" class="font-semibold">-$0.00</span></span>
                            <span class="text-gray-600">{{ __('IVA 16%:') }} <span id="iva" class="font-semibold">$0.00</span></span>
                            <span class="text-gray-800 font-semibold">{{ __('Total a pagar:') }} <span id="total" class="font-semibold">$0.00</span></span>
                        </div>
                    </div>
                    
                    <!-- Proveedor y detalles de la compra -->
                    <div>
                        <h3 class="text-lg font-semibold text-gray-800 mb-4">{{ __('Detalles de la Compra') }}</h3>
                        <div class="mb-4">
                            <label for="id_cliente" class="block text-gray-700">Cliente</label>
                            <select name="id_cliente" id="id_cliente" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200">
                                @foreach($clientes as $cliente)
                                <option value="{{ $cliente->id }}">{{ $cliente->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-4">
                            <label for="fecha_cot" class="block text-gray-700">{{ __('Fecha de Cotización') }}</label>
                            <input type="date" id="fecha_cot" name="fecha_cot" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}">
                        </div>
                        <div class="mb-4">
                            <label for="comentarios" class="block text-gray-700">{{ __('Comentarios') }}</label>
                            <textarea id="comentarios" name="comentarios" rows="4" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200"></textarea>
                        </div>
                        <input type="hidden" name="total" id="input-total">
                        
                    </div>
                </div>
                <div class="flex justify-end items-center mt-6">
                    <a href="{{ route('compras.index') }}" class="bg-gray-300 text-gray-700 px-4 py-2 rounded-lg hover:bg-gray-400 focus:outline-none focus:ring focus:ring-gray-200 transition duration-200 mr-2">Cancelar</a>
                    <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Crear Cotización</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        let productosSeleccionados = [];
        const buscarProductoInput = document.getElementById('buscar-producto');
        const resultadosBusquedaDiv = document.getElementById('resultados-busqueda');
        const productosSeleccionadosTbody = document.getElementById('productos-seleccionados');
        
        buscarProductoInput.addEventListener('input', function() {
            const query = buscarProductoInput.value;
            if (query.length > 2) {
                fetch(`/api/productos?query=${query}`)
                .then(response => response.json())
                .then(data => {
                    renderProductosBuscados(data);
                });
            } else {
                resultadosBusquedaDiv.innerHTML = '';
            }
        });
        
        function renderProductosBuscados(productos) {
            resultadosBusquedaDiv.innerHTML = '';
            productos.forEach(producto => {
                const productoDiv = document.createElement('div');
                productoDiv.classList.add('flex', 'justify-between', 'items-center', 'bg-gray-100', 'p-2', 'rounded-lg', 'mb-2');
                productoDiv.innerHTML = `
                <span>${producto.nombre}</span>
                <button type="button" class="bg-green-500 text-white px-4 py-1 rounded-lg hover:bg-green-600 focus:outline-none" onclick="agregarProducto(${producto.id_producto}, '${producto.nombre}', ${producto.PV})">Agregar</button>
            `;
                resultadosBusquedaDiv.appendChild(productoDiv);
            });
        }
        
        window.agregarProducto = function(id_producto, nombre, PV) {
            const productoExistente = productosSeleccionados.find(producto => producto.id_producto === id_producto);
            if (productoExistente) {
                productoExistente.cantidad += 1;
            } else {
                productosSeleccionados.push({ id_producto, nombre, PV, cantidad: 1 });
            }
            renderProductosSeleccionados();
            calcularTotales();
        }
        
        function renderProductosSeleccionados() {
            productosSeleccionadosTbody.innerHTML = '';
            productosSeleccionados.forEach((producto, index) => {
                const row = document.createElement('tr');
                row.innerHTML = `
                <td class="px-4 py-2">${producto.nombre}</td>
                <td class="px-4 py-2">
                    <input type="number" value="${producto.cantidad}" class="border-gray-300 rounded-lg w-20 text-center" min="1" onchange="actualizarCantidad(${index}, this.value)">
                </td>
                <td class="px-4 py-2">$${producto.PV.toFixed(2)}</td>
                <td class="px-4 py-2">
                    <button type="button" class="text-red-500" onclick="eliminarProducto(${index})">Eliminar</button>
                </td>
            `;
                productosSeleccionadosTbody.appendChild(row);
            });
        }
        
        window.actualizarCantidad = function(index, cantidad) {
            productosSeleccionados[index].cantidad = parseInt(cantidad);
            calcularTotales();
        }
        
        window.eliminarProducto = function(index) {
            productosSeleccionados.splice(index, 1);
            renderProductosSeleccionados();
            calcularTotales();
        }
        
        function calcularTotales() {
            let subtotal = 0;
            productosSeleccionados.forEach(producto => {
                subtotal += producto.PV * producto.cantidad;
            });
            const descuento = 0; // se puede agregar un campo de descuento
            const iva = subtotal * 0.16;
            const total = subtotal + iva - descuento;
            
            document.getElementById('subtotal').textContent = `$${subtotal.toFixed(2)}`;
            document.getElementById('descuento').textContent = `-$${descuento.toFixed(2)}`;
            document.getElementById('iva').textContent = `$${iva.toFixed(2)}`;
            document.getElementById('total').textContent = `$${total.toFixed(2)}`;
            
            // Actualiza el valor del input hidden con el total
            document.getElementById('input-total').value = total.toFixed(2);
        }
            
        
        document.getElementById('cotizacion-form').addEventListener('submit', function(e) {
            productosSeleccionados.forEach((producto, index) => {
                const input = document.createElement('input');
                input.type = 'hidden';
                input.name = `productos[${index}][id_producto]`;
                input.value = producto.id_producto;
                this.appendChild(input);
                
                const inputCantidad = document.createElement('input');
                inputCantidad.type = 'hidden';
                inputCantidad.name = `productos[${index}][cantidad]`;
                inputCantidad.value = producto.cantidad;
                this.appendChild(inputCantidad);
                
                const inputPrecio = document.createElement('input');
                inputPrecio.type = 'hidden';
                inputPrecio.name = `productos[${index}][precio]`;
                
                inputPrecio.value = producto.PV;
                this.appendChild(inputPrecio);
            });
        });
    });
</script>
@endsection
