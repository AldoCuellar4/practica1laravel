@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Lista de Cotizaciones</h2>
        <a href="{{ route('cotizaciones.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Crear Cotización</a>
    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Cliente</th>
                        <th class="px-6 py-3 border">Fecha de Cotización</th>
                        <th class="px-6 py-3 border">Total</th>
                        <th class="px-6 py-3 border">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cotizaciones as $cotizacion)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $cotizacion->id }}</td>
                        <td class="px-6 py-4 border">{{ $cotizacion->cliente->nombre }}</td>
                        <td class="px-6 py-4 border">{{ \Carbon\Carbon::parse($cotizacion->fecha_cot)->format('d-m-Y') }}</td>
                        <td class="px-6 py-4 border">{{ number_format($cotizacion->total, 2) }}</td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('cotizaciones.pdf', $cotizacion->id) }}" class="bg-blue-500 text-white px-4 py-2 rounded-full text-sm mr-2 hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Descargar PDF</a>
                            <form action="{{ route('cotizaciones.destroy', $cotizacion->id) }}" method="POST" class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="bg-red-500 text-white px-4 py-2 rounded-full text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
