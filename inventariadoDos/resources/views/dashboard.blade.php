@extends('layouts.app')

@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
        {{ __('Dashboard') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-full mx-auto sm:px-6 lg:px-8">
        <!-- Primera fila de cards -->
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
            <!-- Card 1 -->
            <a href="{{ route('ventas.create') }}" class="block bg-white overflow-hidden shadow-sm rounded-lg transition-shadow duration-300 hover:shadow-lg">
                <div class="p-6">
                    <h3 class="text-lg font-semibold text-black-900 dark:text-black-100">Agregar Venta</h3>
                    <center>
                        <img class="h-auto max-w-full rounded-lg" src="https://www.pngkey.com/png/full/94-944227_icono-dinero-prstamo-banknote-svg.png" alt="" height="100" width="200px">
                    </center>
                </div>
            </a>
            <!-- Card 2 -->
            <a href="{{ route('compras.create') }}" class="block bg-white overflow-hidden shadow-sm rounded-lg transition-shadow duration-300 hover:shadow-lg">
                <div class="p-6">
                    <h3 class="text-lg font-semibold text-black-900 dark:text-black-100">Nueva Compra</h3>
                    <center>
                        <img class="h-auto max-w-full rounded-lg" src="https://static.vecteezy.com/ti/vetor-gratis/p3/583535-saco-de-compras-on-line-checkout-icon-vector-gr%C3%A1tis-vetor.jpg" alt="" height="100" width="200px">
                    </center>
                </div>
            </a>
            <!-- Card 3 -->
            <a href="{{ route('proveedores.create') }}" class="block bg-white overflow-hidden shadow-sm rounded-lg transition-shadow duration-300 hover:shadow-lg">
                <div class="p-6">
                    <h3 class="text-lg font-semibold text-black-900 dark:text-black-100 text-center">Crear Proveedor</h3>
                    <center>
                        <img class="h-auto max-w-full rounded-lg" src="https://cdn-icons-png.flaticon.com/128/6400/6400287.png" alt="" height="100" width="200px">
                    </center>
                </div>
            </a>
        </div>

        <!-- Segunda fila de cards -->
        <div class="mt-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
            <!-- Card 4 -->
            <a href="{{ route('categorias.create') }}" class="block bg-white overflow-hidden shadow-sm rounded-lg transition-shadow duration-300 hover:shadow-lg">
                <div class="p-6">
                    <h3 class="text-lg font-semibold text-black-900 dark:text-black-100 text-center">Crear Categoria</h3>
                    <center>
                        <img class="h-auto max-w-full rounded-lg" src="https://icon-library.com/images/product-category-icon/product-category-icon-1.jpg" alt="" height="90" width="180px">
                    </center>
                </div>
            </a>
            <!-- Card 5 -->
            <a href="{{ route('clientes.create') }}" class="block bg-white overflow-hidden shadow-sm rounded-lg transition-shadow duration-300 hover:shadow-lg">
                <div class="p-6">
                    <h3 class="text-lg font-semibold text-black-900 dark:text-black-100">Crear Cliente</h3>
                    <center>
                        <img class="h-auto max-w-full rounded-lg" src="https://cdn-icons-png.flaticon.com/128/15862/15862013.png" alt="" height="90" width="180px">
                    </center>
                </div>
            </a>
            <!-- Card 6 -->
            <a href="{{ route('productos.create') }}" class="block bg-white overflow-hidden shadow-sm rounded-lg transition-shadow duration-300 hover:shadow-lg">
                <div class="p-6">
                    <h3 class="text-lg font-semibold text-black-900 dark:text-black-100 text-center">Crear producto</h3>
                    <center>
                        <img class="h-auto max-w-full rounded-lg" src="https://cdn-icons-png.flaticon.com/512/992/992880.png" alt="" height="100" width="200px">
                    </center>
                </div>
            </a>
        </div>
    </div>
</div>

@endsection
