@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="bg-gray-800 text-white px-6 py-4">
            <h2 class="text-2xl font-semibold">{{ __('Editar Inventario') }}</h2>
        </div>
        <div class="p-6">
            <form action="{{ route('inventarios.update', $inventario->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="mb-6">
                    <label for="id_producto" class="block text-gray-700 text-sm font-semibold mb-2">Producto:</label>
                    <select name="id_producto" id="id_producto" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        @foreach($productos as $producto)
                            <option value="{{ $producto->id_producto }}" {{ $producto->id_producto == $inventario->id_producto ? 'selected' : '' }}>{{ $producto->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-6">
                    <label for="id_cat" class="block text-gray-700 text-sm font-semibold mb-2">Categoría:</label>
                    <select name="id_cat" id="id_cat" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        @foreach($categorias as $categoria)
                            <option value="{{ $categoria->id }}" {{ $categoria->id == $inventario->id_cat ? 'selected' : '' }}>{{ $categoria->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-6">
                    <label for="fecha_entrada" class="block text-gray-700 text-sm font-semibold mb-2">Fecha de Entrada:</label>
                    <input type="date" id="fecha_entrada" name="fecha_entrada" value="{{ $inventario->fecha_entrada ? $inventario->fecha_entrada->format('Y-m-d') : '' }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                </div>
                <div class="mb-6">
                    <label for="fecha_salida" class="block text-gray-700 text-sm font-semibold mb-2">Fecha de Salida:</label>
                    <input type="date" id="fecha_salida" name="fecha_salida" value="{{ $inventario->fecha_salida ? $inventario->fecha_salida->format('Y-m-d') : '' }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200">
                </div>
                <div class="mb-6">
                    <label for="movimiento" class="block text-gray-700 text-sm font-semibold mb-2">Movimiento:</label>
                    <input type="text" id="movimiento" name="movimiento" value="{{ $inventario->movimiento }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                </div>
                <div class="mb-6">
                    <label for="motivo" class="block text-gray-700 text-sm font-semibold mb-2">Motivo:</label>
                    <input type="text" id="motivo" name="motivo" value="{{ $inventario->motivo }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                </div>
                <div class="mb-6">
                    <label for="cantidad" class="block text-gray-700 text-sm font-semibold mb-2">Cantidad:</label>
                    <input type="number" id="cantidad" name="cantidad" value="{{ $inventario->cantidad }}" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                </div>
                <div class="flex justify-end space-x-4">
                    <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg mb-6 inline-block hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Actualizar</button>
                    <a href="{{ route('inventarios.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
