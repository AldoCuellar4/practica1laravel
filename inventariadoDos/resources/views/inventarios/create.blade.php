@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="bg-gray-800 text-white px-6 py-4">
            <h2 class="text-2xl font-semibold">{{ __('Agregar Inventario') }}</h2>
        </div>
        <div class="p-6">
            <form action="{{ route('inventarios.store') }}" method="POST">
                @csrf
                <div class="mb-6">
                    <label for="id_producto" class="block text-gray-700 text-sm font-semibold mb-2">Producto:</label>
                    <select name="id_producto" id="id_producto" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" required>
                        @foreach($productos as $producto)
                            <option value="{{ $producto->id_producto }}" data-cat="{{ $producto->id_cat }}">{{ $producto->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <!-- Campo oculto para id_cat -->
                <input type="hidden" name="id_cat" id="id_cat_hidden" value="">
                
                <div class="mb-6">
                    <label for="id_cat_visible" class="block text-gray-700 text-sm font-semibold mb-2">Categoría:</label>
                    <select id="id_cat_visible" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" disabled>
                        @foreach($categorias as $categoria)
                            <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="mb-6" id="entrada-container">
                    <label for="fecha_entrada" class="block text-gray-700 text-sm font-semibold mb-2">Fecha de Entrada:</label>
                    <input type="date" id="fecha_entrada" name="fecha_entrada" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('fecha_entrada') }}">
                </div>
                <div class="mb-6 hidden" id="salida-container">
                    <label for="fecha_salida" class="block text-gray-700 text-sm font-semibold mb-2">Fecha de Salida:</label>
                    <input type="date" id="fecha_salida" name="fecha_salida" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('fecha_salida') }}">
                </div>
                <div class="mb-6">
                    <label for="movimiento" class="block text-gray-700 text-sm font-semibold mb-2">Movimiento:</label>
                    <select id="movimiento" name="movimiento" class="w-full border border-gray-300 rounded-md shadow-sm p-2 focus:ring-indigo-500 focus:border-indigo-500 @error('movimiento') border-red-500 @enderror">
                        <option value="">Tipo de movimiento</option>
                        <option value="1" {{ old('movimiento') == '1' ? 'selected' : '' }}>Entrada</option>
                        <option value="2" {{ old('movimiento') == '2' ? 'selected' : '' }}>Salida</option>
                    </select>
                    @error('movimiento')
                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="motivo" class="block text-gray-700 text-sm font-semibold mb-2">Motivo:</label>
                    <input type="text" id="motivo" name="motivo" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('motivo') }}" required>
                    @error('motivo')
                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="cantidad" class="block text-gray-700 text-sm font-semibold mb-2">Cantidad:</label>
                    <input type="number" id="cantidad" name="cantidad" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ old('cantidad') }}" required>
                    @error('cantidad')
                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex justify-end space-x-4">
                    <a href="{{ route('inventarios.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                    <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const productoSelect = document.getElementById('id_producto');
        const categoriaSelect = document.getElementById('id_cat_visible');
        const hiddenCatInput = document.getElementById('id_cat_hidden');
        const movimientoSelect = document.getElementById('movimiento');
        const entradaContainer = document.getElementById('entrada-container');
        const salidaContainer = document.getElementById('salida-container');

        productoSelect.addEventListener('change', function() {
            const selectedOption = productoSelect.options[productoSelect.selectedIndex];
            const categoryId = selectedOption.getAttribute('data-cat');
            categoriaSelect.value = categoryId;
            hiddenCatInput.value = categoryId; // Update the hidden input value
        });

        movimientoSelect.addEventListener('change', function() {
            const selectedValue = movimientoSelect.value;
            if (selectedValue == '1') {
                entradaContainer.classList.remove('hidden');
                salidaContainer.classList.add('hidden');
            } else if (selectedValue == '2') {
                entradaContainer.classList.add('hidden');
                salidaContainer.classList.remove('hidden');
            } else {
                entradaContainer.classList.add('hidden');
                salidaContainer.classList.add('hidden');
            }
        });

        // Initialize visibility based on the current selection
        movimientoSelect.dispatchEvent(new Event('change'));

        // Ensure category select is enabled before submitting the form
        const form = document.querySelector('form');
        form.addEventListener('submit', function() {
            categoriaSelect.disabled = false;
        });
    });
</script>

@endsection
