@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Lista de Inventarios</h2>
        <a href="{{ route('inventarios.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Agregar Inventario</a>
        <a href="{{ route('inventarios.pdf', ['param' => '2024']) }}" class="bg-green-500 text-white px-6 py-2 rounded-lg hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Exportar PDF</a>
    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Producto</th>
                        <th class="px-6 py-3 border">Categoría</th>
                        <th class="px-6 py-3 border">Fecha de Entrada</th>
                        <th class="px-6 py-3 border">Fecha de Salida</th>
                        <th class="px-6 py-3 border">Movimiento</th>
                        <th class="px-6 py-3 border">Motivo</th>
                        <th class="px-6 py-3 border">Cantidad</th>
                        <th class="px-6 py-3 border text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($inventarios as $inventario)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $inventario->id }}</td>
                        <td class="px-6 py-4 border">{{ $inventario->producto->nombre }}</td>
                        <td class="px-6 py-4 border">{{ $inventario->categoria->nombre }}</td>
                        <td class="px-6 py-4 border">
                            @if($inventario->fecha_entrada)
                                {{ $inventario->fecha_entrada->format('d-m-Y') }}
                            @endif
                        </td>
                        <td class="px-6 py-4 border">
                            @if($inventario->fecha_salida)
                                {{ $inventario->fecha_salida->format('d-m-Y') }}
                            @else
                                N/A
                            @endif
                        </td>
                        <td class="px-6 py-4 border">{{ $inventario->movimiento }}</td>
                        <td class="px-6 py-4 border">{{ $inventario->motivo }}</td>
                        <td class="px-6 py-4 border text-center">{{ $inventario->cantidad }}</td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('inventarios.edit', $inventario->id) }}" class="bg-yellow-500 text-white px-4 py-2 rounded-full text-sm mr-2 hover:bg-yellow-600 focus:outline-none focus:ring focus:ring-yellow-300 transition duration-200">Editar</a>
                            <form action="{{ route('inventarios.destroy', $inventario->id) }}" method="POST" class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="bg-red-500 text-white px-4 py-2 rounded-full text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
