<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Alpine.js -->
    <script src="https://cdn.jsdelivr.net/npm/alpinejs@2.8.2" defer></script>
</head>
<body class="font-sans antialiased bg-white">
    <div x-data="{ open: true, dropdownOpen: false }" class="min-h-screen flex">
        <!-- Sidebar -->
        <nav class="bg-gray-800 text-gray-200 w-64 h-screen fixed transition-transform transform" :class="{'-translate-x-full': !open, 'translate-x-0': open}">
            <div class="flex flex-col h-full">
                <!-- Logo -->
                <div class="flex items-center justify-between p-4 border-b border-gray-700">
                    <a href="{{ route('dashboard') }}">
                        <x-application-logo class="block h-9 w-auto fill-current text-white" />
                    </a>
                    <button @click="open = !open" class="text-gray-200 focus:outline-none lg:hidden">
                        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path :class="{'hidden': open, 'inline-flex': !open}" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16m-7 6h7" />
                            <path :class="{'hidden': !open, 'inline-flex': open}" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                </div>

                <!-- Navigation Links -->
                <div class="flex-grow p-4 space-y-2">
                    <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M13 5v6h6m-6 4v6h6v-6m-6 0H7v6h6v-6z"></path></svg>
                        {{ __('Dashboard') }}
                    </x-nav-link>
                    <x-nav-link :href="route('categorias.index')" :active="request()->routeIs('categorias.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h2a1 1 0 011 1v10a1 1 0 01-1 1H3a1 1 0 01-1-1V11a1 1 0 011-1zM10 3h4a1 1 0 011 1v17a1 1 0 01-1 1h-4a1 1 0 01-1-1V4a1 1 0 011-1zM19 14h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6a1 1 0 011-1z"></path></svg>
                        {{ __('Categorías') }}
                    </x-nav-link>
                    <x-nav-link :href="route('productos.index')" :active="request()->routeIs('productos.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 13V5a2 2 0 00-2-2H6a2 2 0 00-2 2v8m16 0H4m0 0v5a2 2 0 002 2h12a2 2 0 002-2v-5m-2 0v5m-10 0v-5"></path></svg>
                        {{ __('Productos') }}
                    </x-nav-link>
                    <x-nav-link :href="route('proveedores.index')" :active="request()->routeIs('proveedores.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8c-1.1 0-2 .9-2 2m2 2c1.1 0 2-.9 2-2m-2 0a2 2 0 110-4m0 8a2 2 0 100-4m-6 2c0 2 2 4 2 4m4 4s-2-2-2-4m-6-2s2-2 2-4m16 4s-2-2-2-4m-4-4s2 2 2 4m-2 4s2-2 2-4m-8 4s-2 2-2 4m0-8s2-2 2-4"></path></svg>
                        {{ __('Proveedores') }}
                    </x-nav-link>
                    <x-nav-link :href="route('inventarios.index')" :active="request()->routeIs('inventarios.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 9l6 6 6-6"></path></svg>
                        {{ __('Inventario') }}
                    </x-nav-link>
                    <x-nav-link :href="route('compras.index')" :active="request()->routeIs('compras.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 4a1 1 0 011-1h3.28a1 1 0 01.958.707L8.91 5H19a1 1 0 011 1v2a1 1 0 01-1 1H8l-.38 2.238A1 1 0 006.62 11H4a1 1 0 01-1-1V4z"></path></svg>
                        {{ __('Compras') }}
                    </x-nav-link>
                    <x-nav-link :href="route('cotizaciones.index')" :active="request()->routeIs('cotizaciones.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6V4m0 2v2m0 4v2m0 4v2m8-4H4"></path></svg>
                        {{ __('Cotizaciones') }}
                    </x-nav-link>
                    <x-nav-link :href="route('clientes.index')" :active="request()->routeIs('clientes.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zm-8 6a6 6 0 00-6 6v1a1 1 0 001 1h14a1 1 0 001-1v-1a6 6 0 00-6-6z"></path></svg>
                        {{ __('Cliente') }}
                    </x-nav-link>
                    <x-nav-link :href="route('formas_de_pago.index')" :active="request()->routeIs('formas_de_pago.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 9V7a2 2 0 00-2-2H9a2 2 0 00-2 2v2H5v2h14V9h-2zm-6 4v2m-2-2v2m4-2v2m2 2h-8m8 0a2 2 0 002-2v-2a2 2 0 00-2-2m-8 4a2 2 0 01-2-2v-2a2 2 0 012-2"></path></svg>
                        {{ __('Formas de Pago') }}
                    </x-nav-link>
                    <x-nav-link :href="route('vendedores.index')" :active="request()->routeIs('vendedores.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 14l9-5-9-5-9 5 9 5zm0 0v7"></path></svg>
                        {{ __('Vendedores') }}
                    </x-nav-link>
                    <x-nav-link :href="route('ventas.index')" :active="request()->routeIs('ventas.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                        <svg class="w-5 h-5 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16V6a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2zm-1 4a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                        {{ __('Ventas') }}
                    </x-nav-link>
                </div>

<!-- Settings Dropdown -->
<div class="p-4 border-t border-gray-700 relative">
    <button @click="dropdownOpen = !dropdownOpen" class="flex items-center text-sm font-medium text-white hover:text-gray-400 focus:outline-none focus:text-gray-400 transition duration-150 ease-in-out">
        <div>{{ Auth::user()->name }}</div>
        <div class="ml-1">
            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
            </svg>
        </div>
    </button>
    <div x-show="dropdownOpen" @click.away="dropdownOpen = false" class="absolute right-0 bottom-full mb-2 w-48 bg-red border border-gray-200 rounded-md shadow-lg py-1 z-50">
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <x-dropdown-link :href="route('logout')"
                    onclick="event.preventDefault();
                                this.closest('form').submit();">
                {{ __('Log Out') }}
            </x-dropdown-link>
        </form>
    </div>
</div>


            </div>
        </nav>

        <!-- Page Content -->
        <div class="flex-grow p-4 transition-transform transform" :class="{'ml-0': !open, 'ml-64': open}">
            <button @click="open = !open" class="bg-gray-800 text-gray-200 p-2 rounded-md focus:outline-none">
                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path :class="{'hidden': open, 'inline-flex': !open}" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16m-7 6h7" />
                    <path :class="{'hidden': !open, 'inline-flex': open}" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </button>
            @isset($header)
                <header class="bg-white shadow">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endisset

            <main>
                @yield('content')
            </main>
        </div>
    </div>
</body>
</html>
