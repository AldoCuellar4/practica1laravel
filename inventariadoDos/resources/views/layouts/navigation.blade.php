<div class="flex">
    <!-- Sidebar -->
    <nav x-data="{ open: false }" class="bg-gray-800 text-gray-200 w-64 h-screen fixed transition-transform transform lg:translate-x-0" :class="{'-translate-x-full': !open, 'translate-x-0': open}">
        <div class="flex flex-col h-full">
            <!-- Logo -->
            <div class="flex items-center justify-between p-4 border-b border-gray-700">
                <a href="{{ route('dashboard') }}">
                    <x-application-logo class="block h-9 w-auto fill-current text-white" />
                </a>
                <button @click="open = !open" class="text-gray-200 focus:outline-none lg:hidden">
                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path :class="{'hidden': open, 'inline-flex': !open}" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16m-7 6h7" />
                        <path :class="{'hidden': !open, 'inline-flex': open}" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>

            <!-- Navigation Links -->
            <div class="flex-grow p-4 space-y-2">
                <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6H20M4 12H20M4 18H20" />
                    </svg>
                    {{ __('Dashboard') }}
                </x-nav-link>
                <x-nav-link :href="route('categorias.index')" :active="request()->routeIs('categorias.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h18v18H3V3z" />
                    </svg>
                    {{ __('Categorías') }}
                </x-nav-link>
                <x-nav-link :href="route('productos.index')" :active="request()->routeIs('productos.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6H20M4 12H20m-7 6H20" />
                    </svg>
                    {{ __('Productos') }}
                </x-nav-link>
                <x-nav-link :href="route('proveedores.index')" :active="request()->routeIs('proveedores.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h18v18H3V3z" />
                    </svg>
                    {{ __('Proveedores') }}
                </x-nav-link>
                <x-nav-link :href="route('inventarios.index')" :active="request()->routeIs('inventarios.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6H20M4 12H20M4 18H20" />
                    </svg>
                    {{ __('Inventario') }}
                </x-nav-link>
                <x-nav-link :href="route('compras.index')" :active="request()->routeIs('compras.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3v18h18V3H3zM4 5h16v14H4V5z" />
                    </svg>
                    {{ __('Compras') }}
                </x-nav-link>
                <x-nav-link :href="route('cotizaciones.index')" :active="request()->routeIs('cotizaciones.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6H20M4 12H20M4 18H20" />
                    </svg>
                    {{ __('Cotizaciones') }}
                </x-nav-link>
                <x-nav-link :href="route('clientes.index')" :active="request()->routeIs('clientes.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3v18h18V3H3zM4 5h16v14H4V5z" />
                    </svg>
                    {{ __('Cliente') }}
                </x-nav-link>
                <x-nav-link :href="route('formas_de_pago.index')" :active="request()->routeIs('formas_de_pago.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3v18h18V3H3zM4 5h16v14H4V5z" />
                    </svg>
                    {{ __('Formas de Pago') }}
                </x-nav-link>
                <x-nav-link :href="route('vendedores.index')" :active="request()->routeIs('vendedores.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3v18h18V3H3zM4 5h16v14H4V5z" />
                    </svg>
                    {{ __('Vendedores') }}
                </x-nav-link>
                <x-nav-link :href="route('ventas.index')" :active="request()->routeIs('ventas.*')" class="flex items-center text-white py-2 px-4 rounded hover:bg-gray-700">
                    <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3v18h18V3H3zM4 5h16v14H4V5z" />
                    </svg>
                    {{ __('Ventas') }}
                </x-nav-link>
            </div>

            <!-- Settings Dropdown -->
            <div class="p-4 border-t border-gray-700">
                <x-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm font-medium text-white hover:text-gray-400 focus:outline-none focus:text-gray-400 transition duration-150 ease-in-out">
                            <div>{{ Auth::user()->name }}</div>
                            <div class="ml-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>
                    <x-slot name="content">
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <x-dropdown-link :href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Log Out') }}
                            </x-dropdown-link>
                        </form>
                    </x-slot>
                </x-dropdown>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="flex-grow p-4 ml-64">
        <button @click="open = !open" class="bg-gray-800 text-gray-200 p-2 rounded-md lg:hidden focus:outline-none">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path :class="{'hidden': open, 'inline-flex': !open}" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16m-7 6h7" />
                <path :class="{'hidden': !open, 'inline-flex': open}" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </button>
        @yield('content')
    </div>
</div>
