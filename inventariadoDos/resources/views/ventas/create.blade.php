@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="px-6 py-4">
            <h2 class="text-2xl font-semibold text-gray-800 mb-6">{{ __('Nueva Venta') }}</h2>
            
            <form action="{{ route('ventas.store') }}" method="POST" id="venta-form">
                @csrf
                <div class="grid grid-cols-1 md:grid-cols-2 gap-6">
                    <!-- Productos -->
                    <div>
                        <h3 class="text-lg font-semibold text-gray-800 mb-4">{{ __('Productos') }}</h3>
                        <div class="mb-4">
                            <input type="text" id="buscar-producto" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200" placeholder="Buscar por nombre, categoría, SKU o código de barras">
                            <div id="resultados-busqueda" class="mt-2"></div>
                        </div>
                        
                        <!-- Tabla de productos seleccionados -->
                        <div class="mt-4">
                            <table class="w-full text-left table-auto">
                                <thead>
                                    <tr>
                                        <th class="px-4 py-2">{{ __('Nombre') }}</th>
                                        <th class="px-4 py-2">{{ __('Cantidad') }}</th>
                                        <th class="px-4 py-2">{{ __('Precio') }}</th>
                                        <th class="px-4 py-2">{{ __('Acción') }}</th>
                                    </tr>
                                </thead>
                                <tbody id="productos-seleccionados">
                                    <!-- Las filas de productos seleccionados se añadirán aquí -->
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- Totales -->
                        <div class="flex flex-col sm:flex-row justify-between items-start sm:items-center mt-6">
                            <span class="text-gray-600">{{ __('Subtotal:') }} <span id="subtotal" class="font-semibold">$0.00</span></span>
                            <span class="text-gray-600">{{ __('Descuento:') }} <span id="descuento" class="font-semibold">-$0.00</span></span>
                            <span class="text-gray-600">{{ __('IVA 16%:') }} <span id="iva" class="font-semibold">$0.00</span></span>
                            <span class="text-gray-800 font-semibold">{{ __('Total a pagar:') }} <span id="total" class="font-semibold">$0.00</span></span>
                        </div>
                    </div>
                    
                    <!-- Vendedor y Cliente -->
                    <div>
                        <h3 class="text-lg font-semibold text-gray-800 mb-4">{{ __('Detalles de la Venta') }}</h3>
                        <div class="mb-4">
                            <label for="id_vendedor" class="block text-gray-700">{{ __('Vendedor') }}</label>
                            <select id="id_vendedor" name="id_vendedor" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200" required>
                                @foreach($vendedores as $vendedor)
                                <option value="{{ $vendedor->id }}">{{ $vendedor->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-4">
                            <label for="id_cliente" class="block text-gray-700">{{ __('Cliente') }}</label>
                            <select id="id_cliente" name="id_cliente" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200" required>
                                @foreach($clientes as $cliente)
                                <option value="{{ $cliente->id }}">{{ $cliente->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-4">
                            <label for="monto" class="block text-gray-700">{{ __('Monto') }}</label>
                            <input type="number" id="monto" name="monto" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200" step="0.01" min="0" required>
                        </div>
                        <div class="mb-4">
                            <label for="fecha_venta" class="block text-gray-700">{{ __('Fecha de Venta') }}</label>
                            <input type="date" id="fecha_venta" name="fecha_venta" class="w-full border-gray-300 rounded-lg focus:ring focus:ring-blue-200 transition duration-200" required>
                        </div>
                    </div>
                </div>
                <div class="flex justify-end items-center mt-6">
                    <a href="{{ route('ventas.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                    <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
document.addEventListener('DOMContentLoaded', function() {
    let productosSeleccionados = [];
    const buscarProductoInput = document.getElementById('buscar-producto');
    const resultadosBusquedaDiv = document.getElementById('resultados-busqueda');
    const productosSeleccionadosDiv = document.getElementById('productos-seleccionados');
    const montoInput = document.getElementById('monto');

    
    buscarProductoInput.addEventListener('input', function() {
        const query = buscarProductoInput.value;
        if (query.length > 2) {
            fetch(`/api/productos?query=${query}`)
            .then(response => response.json())
            .then(data => {
                renderProductosBuscados(data);
            });
        } else {
            resultadosBusquedaDiv.innerHTML = '';
        }
    });
    
    function renderProductosBuscados(productos) {
        resultadosBusquedaDiv.innerHTML = '';
        productos.forEach(producto => {
            const productoDiv = document.createElement('div');
            productoDiv.classList.add('flex', 'justify-between', 'items-center', 'bg-gray-100', 'p-2', 'rounded-lg', 'mb-2');
            productoDiv.innerHTML = `
            <span>${producto.nombre}</span>
            <button type="button" class="bg-green-500 text-white px-4 py-1 rounded-lg hover:bg-green-600 focus:outline-none" onclick="agregarProducto(${producto.id_producto}, '${producto.nombre}', ${producto.PV}, ${producto.stock})">Agregar</button>
        `;
            resultadosBusquedaDiv.appendChild(productoDiv);
        });
    }
    
    window.agregarProducto = function(id_producto, nombre, precio, stock) {
        const productoExistente = productosSeleccionados.find(producto => producto.id_producto === id_producto);
        if (productoExistente) {
            alert('El producto ya está en la lista. Por favor, actualice la cantidad si es necesario.');
        } else {
            productosSeleccionados.push({ id_producto, nombre, precio, cantidad: 1, stock });
            renderProductosSeleccionados();
            calcularTotales();
        }
    }
    
    function renderProductosSeleccionados() {
        productosSeleccionadosDiv.innerHTML = '';
        productosSeleccionados.forEach((producto, index) => {
            const productoRow = document.createElement('tr');
            productoRow.innerHTML = `
            <td class="px-4 py-2">${producto.nombre}</td>
            <td class="px-4 py-2">
                <input type="number" value="${producto.cantidad}" class="border-gray-300 rounded-lg w-20 text-center" min="1" max="${producto.stock}" onchange="actualizarCantidad(${index}, this.value)">
            </td>
            <td class="px-4 py-2">${producto.precio}</td>
            <td class="px-4 py-2">
                <button type="button" class="text-red-500" onclick="eliminarProducto(${index})">Eliminar</button>
            </td>
        `;
            productosSeleccionadosDiv.appendChild(productoRow);
        });
    }
    
    window.actualizarCantidad = function(index, cantidad) {
        const producto = productosSeleccionados[index];
        const cantidadInt = parseInt(cantidad);
        if (cantidadInt > producto.stock) {
            alert(`La cantidad no puede exceder el stock disponible de ${producto.stock}.`);
            return;
        }
        producto.cantidad = cantidadInt;
        calcularTotales();
    }
    
    window.eliminarProducto = function(index) {
        productosSeleccionados.splice(index, 1);
        renderProductosSeleccionados();
        calcularTotales();
    }
    
    function calcularTotales() {
        let subtotal = 0;
        productosSeleccionados.forEach(producto => {
            subtotal += producto.precio * producto.cantidad;
        });
        const descuento = 0; 
        const iva = subtotal * 0.16; // 16% IVA
        const total = subtotal + iva - descuento;

        document.getElementById('subtotal').textContent = `$${subtotal.toFixed(2)}`;
        document.getElementById('descuento').textContent = `-$${descuento.toFixed(2)}`;
        document.getElementById('iva').textContent = `$${iva.toFixed(2)}`;
        document.getElementById('total').textContent = `$${total.toFixed(2)}`;
        
        // Actualizar el campo de monto
        montoInput.value = total.toFixed(2);
    }
    
    document.getElementById('venta-form').addEventListener('submit', function(e) {
        // Verificar si el monto ingresado es menor que el total calculado
        const montoIngresado = parseFloat(montoInput.value);
        const totalCalculado = parseFloat(document.getElementById('total').textContent.replace('$', ''));
        
        if (montoIngresado < totalCalculado) {
            e.preventDefault(); // Evitar el envío del formulario
            alert('El monto ingresado es menor que el total calculado. Por favor, ajusta el monto.');
            return;
        } else if (montoIngresado > totalCalculado) {
            const cambio = montoIngresado - totalCalculado;
            alert(`El monto ingresado es mayor que el total. Tu cambio es: $${cambio.toFixed(2)}`);
        }
        
        productosSeleccionados.forEach((producto, index) => {
            const input = document.createElement('input');
            input.type = 'hidden';
            input.name = `productos[${index}][id_producto]`;
            input.value = producto.id_producto;
            this.appendChild(input);
            
            const inputCantidad = document.createElement('input');
            inputCantidad.type = 'hidden';
            inputCantidad.name = `productos[${index}][cantidad]`;
            inputCantidad.value = producto.cantidad;
            this.appendChild(inputCantidad);
            
            const inputPrecio = document.createElement('input');
            inputPrecio.type = 'hidden';
            inputPrecio.name = `productos[${index}][precio]`;
            inputPrecio.value = producto.precio;
            this.appendChild(inputPrecio);
        });
    });
});
</script>

@endsection
