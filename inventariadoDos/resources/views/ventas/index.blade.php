@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Lista de Ventas</h2>
        <a href="{{ route('ventas.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Nueva Venta</a>
    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Vendedor</th>
                        <th class="px-6 py-3 border">Cliente</th>
                        <th class="px-6 py-3 border">Monto</th>
                        <th class="px-6 py-3 border">Fecha de Venta</th>
                        <th class="px-6 py-3 border">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ventas as $venta)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $venta->id }}</td>
                        <td class="px-6 py-4 border">{{ $venta->vendedor->nombre }}</td>
                        <td class="px-6 py-4 border">{{ $venta->cliente->nombre }}</td>
                        <td class="px-6 py-4 border">{{ number_format($venta->monto, 2) }}</td>
                        <td class="px-6 py-4 border">{{ \Carbon\Carbon::parse($venta->fecha_venta)->format('d-m-Y') }}</td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('ventas.pdf', $venta->id) }}" class="bg-green-500 text-white px-4 py-2 rounded-full text-sm mr-2 hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Exportar PDF</a>
                            <form action="{{ route('ventas.destroy', $venta->id) }}" method="POST" class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="bg-red-500 text-white px-4 py-2 rounded-full text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
