<!-- resources/views/ventas/show.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Detalle de Venta</h1>

    <div class="card">
        <div class="card-header">
            Venta #{{ $venta->id }}
        </div>
        <div class="card-body">
            <p><strong>Vendedor:</strong> {{ $venta->vendedor->nombre }}</p>
            <p><strong>Cliente:</strong> {{ $venta->cliente->nombre }}</p>
            <p><strong>Monto:</strong> {{ $venta->monto }}</p>
            <p><strong>Fecha de Venta:</strong> {{ $venta->fecha_venta }}</p>

            <h3>Productos</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($venta->productos as $producto)
                        <tr>
                            <td>{{ $producto->nombre }}</td>
                            <td>{{ $producto->pivot->cantidad }}</td>
                            <td>{{ $producto->pivot->precio }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <a href="{{ route('ventas.index') }}" class="btn btn-primary">Volver a la Lista de Ventas</a>
        </div>
    </div>
</div>
@endsection
