@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-center">
        <div class="w-full max-w-2xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-2xl font-semibold">{{ __('Detalles de la Forma de Pago') }}</h2>
                </div>
                
                <div class="p-6">
                    <div class="mb-4">
                        <strong class="block text-gray-700 text-sm font-semibold">ID:</strong>
                        <span class="text-gray-900">{{ $formaDePago->id }}</span>
                    </div>
                    <div class="mb-4">
                        <strong class="block text-gray-700 text-sm font-semibold">Tipo:</strong>
                        <span class="text-gray-900">{{ $formaDePago->tipo }}</span>
                    </div>
                    <div class="flex justify-end mt-4">
                        <a href="{{ route('formas_de_pago.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Volver</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
