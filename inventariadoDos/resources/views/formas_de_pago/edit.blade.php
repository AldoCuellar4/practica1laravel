@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-center mt-8">
        <div class="w-full max-w-2xl">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <div class="bg-gray-800 text-white px-6 py-4">
                    <h2 class="text-xl font-semibold">{{ __('Editar Forma de Pago') }}</h2>
                </div>

                <div class="p-6">
                    <form action="{{ route('formas_de_pago.update', $formaDePago->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-6">
                            <label for="tipo" class="block text-gray-700 text-sm font-semibold mb-2">Tipo:</label>
                            <input type="text" id="tipo" name="tipo" class="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 transition duration-200" value="{{ $formaDePago->tipo }}" required>
                        </div>
                        <div class="flex justify-end space-x-4">
                            <a href="{{ route('formas_de_pago.index') }}" class="bg-gray-500 text-white px-6 py-2 rounded-lg hover:bg-gray-600 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Cancelar</a>
                            <button type="submit" class="bg-black text-white px-6 py-2 rounded-lg hover:bg-gray-800 focus:outline-none focus:ring focus:ring-gray-300 transition duration-200">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
