@extends('layouts.app')

@section('content')
<div class="container mx-auto px-4 py-8">
    <div class="flex justify-between items-center mb-6">
        <h2 class="text-2xl font-semibold text-gray-800">Listado de Formas de Pago</h2>
        <a href="{{ route('formas_de_pago.create') }}" class="bg-blue-500 text-white px-6 py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Crear Nueva Forma de Pago</a>
        <a href="{{ route('formas_pago.pdf') }}" class="bg-green-500 text-white px-6 py-2 rounded-lg hover:bg-green-600 focus:outline-none focus:ring focus:ring-green-300 transition duration-200">Exportar PDF</a>
    </div>
    <div class="bg-white shadow-lg rounded-lg overflow-hidden">
        @if (session('success'))
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-6" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mb-6" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <div class="overflow-x-auto">
            <table class="min-w-full bg-white border border-gray-300">
                <thead>
                    <tr class="bg-gray-100 text-gray-700">
                        <th class="px-6 py-3 border">ID</th>
                        <th class="px-6 py-3 border">Tipo</th>
                        <th class="px-6 py-3 border text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($formasDePago as $formaDePago)
                    <tr class="bg-white">
                        <td class="px-6 py-4 border text-center">{{ $formaDePago->id }}</td>
                        <td class="px-6 py-4 border">{{ $formaDePago->tipo }}</td>
                        <td class="px-6 py-4 border text-center">
                            <a href="{{ route('formas_de_pago.show', $formaDePago->id) }}" class="bg-blue-500 text-white px-4 py-1 rounded-lg text-sm mr-2 hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300 transition duration-200">Ver</a>
                            <a href="{{ route('formas_de_pago.edit', $formaDePago->id) }}" class="bg-yellow-500 text-white px-4 py-1 rounded-lg text-sm mr-2 hover:bg-yellow-600 focus:outline-none focus:ring focus:ring-yellow-300 transition duration-200">Editar</a>
                            <form action="{{ route('formas_de_pago.destroy', $formaDePago->id) }}" method="POST" class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="bg-red-500 text-white px-4 py-1 rounded-lg text-sm hover:bg-red-600 focus:outline-none focus:ring focus:ring-red-300 transition duration-200" onclick="return confirm('¿Estás seguro de eliminar esta forma de pago?')">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
